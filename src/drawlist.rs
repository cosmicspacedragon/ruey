use color::Color;
use rect::Rect;
use std::vec::Vec;
use vertex::Vertex;

pub struct DrawCommand {
    pub index_offset: u32,
    pub index_count: u32,
    pub clip_rect: Rect,
}

#[derive(Default)]
pub struct DrawList {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,
    pub commands: Vec<DrawCommand>,
    clip_rects: Vec<Rect>,
}

impl DrawList {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn new_command(&mut self) {
        let clip_rect = *self.clip_rects.last().expect("No clip rect! Did you pop one too many?");
        if self.commands.is_empty() {
            self.commands.push(DrawCommand {
                index_offset: self.indices.len() as u32,
                index_count: 0,
                clip_rect,
            });
        } else {
            let last_clip_rect;
            {
                let last_command: &mut DrawCommand = self.commands.last_mut().unwrap();
                last_command.index_count = self.indices.len() as u32 - last_command.index_offset;
                last_clip_rect = last_command.clip_rect;
            }

            if last_clip_rect != clip_rect {
                self.commands.push(DrawCommand {
                    index_offset: self.indices.len() as u32,
                    index_count: 0,
                    clip_rect,
                });
            }
        }
    }

    pub fn push_clip_rect(&mut self, clip_rect: Rect) {
        let new_clip_rect;
        if let Some(last_clip_rect) = self.clip_rects.last() {
            new_clip_rect = Rect::new_intersection(clip_rect, *last_clip_rect);
        } else {
            new_clip_rect = clip_rect;
        }
        self.clip_rects.push(new_clip_rect);
    }

    pub fn pop_clip_rect(&mut self) {
        self.clip_rects.pop();
    }

    pub fn push_quad(&mut self, area: Rect, uv_area: Rect, color: Color) {
        let index_offset = self.vertices.len() as u32;

        self.vertices
            .push(Vertex::new(area.min.x, area.min.y, uv_area.min.x, uv_area.min.y, color));
        self.vertices
            .push(Vertex::new(area.min.x, area.max.y, uv_area.min.x, uv_area.max.y, color));
        self.vertices
            .push(Vertex::new(area.max.x, area.max.y, uv_area.max.x, uv_area.max.y, color));
        self.vertices
            .push(Vertex::new(area.max.x, area.min.y, uv_area.max.x, uv_area.min.y, color));

        self.indices.push(index_offset);
        self.indices.push(index_offset + 1);
        self.indices.push(index_offset + 2);

        self.indices.push(index_offset);
        self.indices.push(index_offset + 2);
        self.indices.push(index_offset + 3);
    }

    pub fn push_colored_quad(&mut self, area: Rect, color: Color) {
        self.push_quad(
            area,
            Rect::new(1.0 / 512.0, 1.0 / 512.0, 1.0 / 512.0, 1.0 / 512.0),
            color,
        );
    }
}
