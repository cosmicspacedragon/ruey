extern crate env_logger;
#[macro_use]
extern crate log;
extern crate gl;
extern crate notify;
extern crate ruey;
extern crate sdl2;
extern crate stopwatch;

use notify::{DebouncedEvent, RecommendedWatcher, RecursiveMode, Watcher};
use ruey::context::drindexprovider::DRIndexProvider;
use ruey::context::Context;
use ruey::drawlist::DrawList;
use ruey::guierror::GuiError;
use ruey::{guikey::GuiKey, rect::Rect, vertex::Vertex, Gui};
use std::collections::BTreeMap;
use std::sync::mpsc::{channel, TryRecvError};
use std::time::Duration;
use std::{ffi::CString, time};
use stopwatch::Stopwatch;

fn init_gl(font_texture_data: &[u8]) -> gl::types::GLuint {
    let mut vao: gl::types::GLuint = 0;
    let mut vertex_buffer: gl::types::GLuint = 0;
    let mut index_buffer: gl::types::GLuint = 0;
    unsafe {
        gl::GenVertexArrays(1, &mut vao);
        gl::BindVertexArray(vao);

        gl::GenBuffers(1, &mut vertex_buffer);
        gl::BindBuffer(gl::ARRAY_BUFFER, vertex_buffer);
        gl::BufferData(
            gl::ARRAY_BUFFER,
            std::mem::size_of::<Vertex>() as isize * 4096 * 4 as gl::types::GLsizeiptr,
            std::ptr::null(),
            gl::DYNAMIC_DRAW,
        );

        gl::GenBuffers(1, &mut index_buffer);
        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, index_buffer);
        gl::BufferData(
            gl::ELEMENT_ARRAY_BUFFER,
            std::mem::size_of::<gl::types::GLint>() as isize * 4096 * 6 as gl::types::GLsizeiptr,
            std::ptr::null(),
            gl::DYNAMIC_DRAW,
        );
    }

    let vertex_shader: gl::types::GLuint;
    unsafe {
        let vertex_shader_source = CString::new(
            "\
             #version 330 core\n\
             layout(location = 0) in vec2 position;\
             layout(location = 1) in vec2 uv;\
             layout(location = 2) in vec4 color;\
             out vec4 vertexColor;\
             out vec2 vertexUV;\
             uniform vec2 resolution;\
             void main(){\
             vertexColor = color;\
             vec2 pos = position / vec2(resolution);\
             pos.y = 1.0f - pos.y;\
             pos -= vec2(0.5f, 0.5f);\
             pos *= 2.0f;\
             gl_Position = vec4(pos, 0.0f, 1.0f);\
             vertexUV = uv;\
             }",
        )
        .unwrap();
        vertex_shader = gl::CreateShader(gl::VERTEX_SHADER);
        gl::ShaderSource(
            vertex_shader,
            1,
            &vertex_shader_source.as_c_str().as_ptr(),
            std::ptr::null(),
        );
        gl::CompileShader(vertex_shader);

        let mut success: gl::types::GLint = 0;
        gl::GetShaderiv(vertex_shader, gl::COMPILE_STATUS, &mut success);
        if success == 0 {
            panic!("Vertex shader compilation failed");
        }
    }

    let fragment_shader: gl::types::GLuint;
    unsafe {
        let fragment_shader_source = CString::new(
            "\
             #version 330 core\n\
             in vec4 vertexColor;\
             in vec2 vertexUV;\
             out vec4 fragmentColor;\
             uniform sampler2D tex;\
             void main(){\
             float col = texture(tex, vertexUV).r;\
             fragmentColor = vec4(vertexColor.rgb, col * vertexColor.a);\
             }",
        )
        .unwrap();
        fragment_shader = gl::CreateShader(gl::FRAGMENT_SHADER);
        gl::ShaderSource(
            fragment_shader,
            1,
            &fragment_shader_source.as_c_str().as_ptr(),
            std::ptr::null(),
        );
        gl::CompileShader(fragment_shader);

        let mut success: gl::types::GLint = 0;
        gl::GetShaderiv(fragment_shader, gl::COMPILE_STATUS, &mut success);
        if success == 0 {
            panic!("Fragment shader compilation failed");
        }
    }

    let program: gl::types::GLuint;
    unsafe {
        program = gl::CreateProgram();
        gl::AttachShader(program, vertex_shader);
        gl::AttachShader(program, fragment_shader);
        gl::LinkProgram(program);

        let mut success: gl::types::GLint = 0;
        gl::GetProgramiv(program, gl::LINK_STATUS, &mut success);
        if success == 0 {
            panic!("Shader program linking error");
        }

        gl::DetachShader(program, vertex_shader);
        gl::DetachShader(program, fragment_shader);
        gl::DeleteShader(vertex_shader);
        gl::DeleteShader(fragment_shader);

        gl::EnableVertexAttribArray(0);
        gl::EnableVertexAttribArray(1);
        gl::EnableVertexAttribArray(2);

        gl::VertexAttribPointer(
            0,
            2,
            gl::FLOAT,
            gl::FALSE,
            std::mem::size_of::<Vertex>() as gl::types::GLsizei,
            std::ptr::null(),
        );
        gl::VertexAttribPointer(
            1,
            2,
            gl::FLOAT,
            gl::FALSE,
            std::mem::size_of::<Vertex>() as gl::types::GLsizei,
            (std::mem::size_of::<f32>() * 2) as *const gl::types::GLvoid,
        );
        gl::VertexAttribPointer(
            2,
            4,
            gl::FLOAT,
            gl::FALSE,
            std::mem::size_of::<Vertex>() as gl::types::GLsizei,
            (std::mem::size_of::<f32>() * 4) as *const gl::types::GLvoid,
        );
        gl::UseProgram(program);
    }

    let mut font_texture: gl::types::GLuint = 0;
    unsafe {
        gl::GenTextures(1, &mut font_texture);
        gl::BindTexture(gl::TEXTURE_2D, font_texture);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT as i32);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT as i32);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
        gl::TexImage2D(
            gl::TEXTURE_2D,
            0,
            gl::R8 as i32,
            512,
            512,
            0,
            gl::RED,
            gl::UNSIGNED_BYTE,
            font_texture_data.as_ptr() as *const std::os::raw::c_void,
        );
        gl::Enable(gl::BLEND);
        gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
        gl::BlendEquation(gl::FUNC_ADD);
    }

    unsafe {
        let err = gl::GetError();
        if err != 0 {
            panic!("GL error after initialization: {}", err);
        }
    }

    program
}

#[allow(clippy::single_match, clippy::cognitive_complexity)]
fn main() {
    env_logger::init();

    let mut resolution_x: f32 = 512.0;
    let mut resolution_y: f32 = 512.0;

    let mut gui;
    match Gui::new(std::path::Path::new("data/fonts/CamingoCode-Bold.ttf")) {
        Ok(new_gui) => gui = new_gui,
        Err(err) => panic!("Error creating gui: {}", err),
    }
    let mut context;
    match Context::new(std::path::Path::new("data/style.ron")) {
        Ok(new_context) => context = new_context,
        Err(err) => panic!("Error creating context: {}", err),
    }

    let mut dropdown_strings: Vec<String> = vec![
        "First option".to_string(),
        "Second".to_string(),
        "Third".to_string(),
    ];
    context.add_list_provider("dropdown_strings", &mut dropdown_strings);

    let mut list_strings: Vec<String> = vec![
        "I am number one".to_string(),
        "I am number two".to_string(),
    ];
    context.add_list_provider("list_strings", &mut list_strings);

    let mut strings_tree: BTreeMap<String, String> = BTreeMap::new();
    strings_tree.insert("dropdown_value".to_string(), "First option".to_string());
    strings_tree.insert("inputstring".to_string(), "Textinput".to_string());
    strings_tree.insert("avg_fps".to_string(), "0".to_string());
    context.add_index_provider("strings", &mut strings_tree);

    let mut add_button = false;
    let mut remove_button = false;
    let mut bools_provider = DRIndexProvider::<bool>::new();
    bools_provider.add("add_button", &mut add_button);
    bools_provider.add("remove_button", &mut remove_button);
    context.add_index_provider("bools", &mut bools_provider);

    let mut gui_status: Result<(), GuiError> =
        match gui.load_file(&mut context, std::path::Path::new("data/testfile.gui.ron")) {
            Ok(_) => {
                gui.build(Rect::new(0.0, 0.0, resolution_x, resolution_y), &mut context);
                Ok(())
            }
            Err(err) => {
                error!("Error creating gui: {}", err);
                Err(err)
            }
        };

    let (file_watch_transmitter, file_watch_receiver) = channel();
    let mut watcher: RecommendedWatcher = Watcher::new(file_watch_transmitter, Duration::from_millis(50)).unwrap();
    watcher.watch("data", RecursiveMode::NonRecursive).unwrap();

    let sdl = sdl2::init().unwrap();
    let sdl_video = sdl.video().unwrap();
    let gl_attr = sdl_video.gl_attr();
    gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
    gl_attr.set_context_version(3, 3);
    let sdl_window = sdl_video
        .window("ruey SDL/GL", resolution_x as u32, resolution_y as u32)
        .resizable()
        .opengl()
        .build()
        .unwrap();

    let _gl_context = sdl_window.gl_create_context().unwrap();
    gl::load_with(|s| sdl_video.gl_get_proc_address(s) as *const std::os::raw::c_void);

    let shader_program = init_gl(gui.get_font_texture());
    let resolution_attrib =
        unsafe { gl::GetUniformLocation(shader_program, CString::new("resolution").unwrap().as_c_str().as_ptr()) };
    unsafe {
        gl::Uniform2f(resolution_attrib, resolution_x, resolution_y);
    }

    unsafe {
        gl::ClearColor(86.0 / 255.0, 120.0 / 255.0, 154.0 / 255.0, 1.0);
    }

    let mut keys_down = std::collections::HashSet::<sdl2::keyboard::Keycode>::new();

    let mut event_pump = sdl.event_pump().unwrap();
    let mut timer = Stopwatch::start_new();

    let mut time_accumulation = 0;
    let mut total_draw_time = 0;
    let mut frame_count = 0;
    'mainloop: loop {
        let mouse_state = sdl2::mouse::MouseState::new(&event_pump);
        for event in event_pump.poll_iter() {
            match event {
                sdl2::event::Event::Quit { .. } => break 'mainloop,
                sdl2::event::Event::Window { win_event, .. } => match win_event {
                    sdl2::event::WindowEvent::Resized(x, y) => {
                        resolution_x = x as f32;
                        resolution_y = y as f32;
                        unsafe {
                            gl::Viewport(0, 0, x, y);
                            gl::Uniform2f(resolution_attrib, x as f32, y as f32);
                        };
                        if gui_status.is_ok() {
                            gui.set_area(Rect::new(0.0, 0.0, resolution_x as f32, resolution_y as f32));
                        }
                    }
                    _ => {}
                },
                sdl2::event::Event::KeyDown { keycode, .. } => {
                    let keycode = keycode.expect("No keycode in KeyDown event");
                    if !context.wants_keyboard_focus() && !keys_down.contains(&keycode) {
                        keys_down.insert(keycode);
                    }
                    if let Some(key) = translate_key(keycode) {
                        gui.key_down(&mut context, key)
                    }
                }
                sdl2::event::Event::KeyUp { keycode, .. } => {
                    let keycode = keycode.expect("No keycode in KeyUp event");
                    keys_down.remove(&keycode);
                    if let Some(key) = translate_key(keycode) {
                        gui.key_up(&mut context, key)
                    }
                }
                sdl2::event::Event::MouseButtonDown { x, y, mouse_btn, .. } => {
                    gui.mouse_down(&mut context, x, y, mouse_btn as i32)
                }
                sdl2::event::Event::MouseButtonUp { x, y, mouse_btn, .. } => {
                    gui.mouse_up(&mut context, x, y, mouse_btn as i32)
                }
                sdl2::event::Event::TextInput { text, .. } => {
                    gui.text_input(&mut context, &text);
                }
                _ => {}
            }
        }
        match file_watch_receiver.try_recv() {
            Ok(event) => match event {
                DebouncedEvent::Write(path) => {
                    if path.to_str().unwrap().contains("style.ron") {
                        info!("Reloading style from data/style.ron");
                        match context.load_style(std::path::Path::new("data/style.ron")) {
                            Ok(_) => {
                                debug!("Successfully reloaded style");
                            }
                            Err(err) => {
                                error!("Error reloading gui: {}", err);
                            }
                        }
                    } else if path.to_str().unwrap().contains("testfile.gui.ron") {
                        info!("Reloading GUI from data/testfile.gui.ron");
                        match gui.load_file(&mut context, std::path::Path::new("data/testfile.gui.ron")) {
                            Ok(_) => {
                                let build_timer = Stopwatch::start_new();
                                context.reset_providers();
                                gui.build(Rect::new(0.0, 0.0, resolution_x, resolution_y), &mut context);
                                debug!(
                                    "Building took {} ms",
                                    build_timer.elapsed().subsec_nanos() as f32 * 0.000_001
                                );
                                gui_status = Ok(());
                            }
                            Err(err) => {
                                error!("Error reloading gui: {}", err);
                                gui_status = Err(err);
                            }
                        }
                    }
                }
                _ => {}
            },
            Err(err) => {
                if err == TryRecvError::Disconnected {
                    panic!("File watcher channel disconnected!")
                }
            }
        }

        if add_button {
            list_strings.push(strings_tree.get("dropdown_value").unwrap().to_string());
        }
        if remove_button {
            list_strings.pop();
        }

        if gui_status.is_ok() {
            let draw_timer = Stopwatch::start_new();
            context.reset_providers();
            let (draw_list, overlay_draw_list) = gui.draw(mouse_state.x(), mouse_state.y(), &mut context);
            total_draw_time += draw_timer.elapsed().subsec_nanos();

            unsafe {
                gl::Disable(gl::SCISSOR_TEST);
                gl::Clear(gl::COLOR_BUFFER_BIT);
                gl::Enable(gl::SCISSOR_TEST);

                draw_draw_list(&draw_list, resolution_y);
                draw_draw_list(&overlay_draw_list, resolution_y);
            }
        } else {
            unsafe {
                gl::Disable(gl::SCISSOR_TEST);
                gl::Clear(gl::COLOR_BUFFER_BIT);
                gl::Enable(gl::SCISSOR_TEST);
            }
        }

        sdl_window.gl_swap_window();

        // Loosely 30 FPS
        let elapsed = timer.elapsed();
        if elapsed.subsec_millis() < 33 {
            let sleep_time = time::Duration::from_millis(33);
            if elapsed < sleep_time {
                std::thread::sleep(sleep_time - elapsed);
            }
        }
        time_accumulation += timer.elapsed().subsec_millis();
        frame_count += 1;
        if time_accumulation >= 1000 {
            *strings_tree.get_mut("avg_fps").unwrap() =
                (total_draw_time as f32 / frame_count as f32 * 0.000_001).to_string();
            total_draw_time = 0;
            time_accumulation = 0;
            frame_count = 0;
        }
        timer.restart();
    }
}

fn draw_draw_list(draw_list: &DrawList, resolution_y: f32) {
    if !draw_list.commands.is_empty() {
        unsafe {
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (draw_list.vertices.len() * std::mem::size_of::<Vertex>()) as gl::types::GLsizeiptr,
                draw_list.vertices.as_ptr() as *const gl::types::GLvoid,
                gl::DYNAMIC_DRAW,
            );
            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                (draw_list.indices.len() * std::mem::size_of::<u32>()) as gl::types::GLsizeiptr,
                draw_list.indices.as_ptr() as *const gl::types::GLvoid,
                gl::DYNAMIC_DRAW,
            );

            for command in &draw_list.commands {
                let clip_rect = command.clip_rect;
                gl::Scissor(
                    clip_rect.min.x as gl::types::GLint,
                    (resolution_y - clip_rect.min.y - clip_rect.size().y) as gl::types::GLint,
                    clip_rect.size().x as gl::types::GLsizei,
                    clip_rect.size().y as gl::types::GLsizei,
                );
                gl::DrawElements(
                    gl::TRIANGLES,
                    command.index_count as gl::types::GLsizei,
                    gl::UNSIGNED_INT,
                    (command.index_offset * std::mem::size_of::<u32>() as u32) as *const std::os::raw::c_void,
                );
            }
        }
    }
}

fn translate_key(keycode: sdl2::keyboard::Keycode) -> Option<GuiKey> {
    match keycode {
        sdl2::keyboard::Keycode::Up => Some(GuiKey::UP),
        sdl2::keyboard::Keycode::Down => Some(GuiKey::DOWN),
        sdl2::keyboard::Keycode::Left => Some(GuiKey::LEFT),
        sdl2::keyboard::Keycode::Right => Some(GuiKey::RIGHT),
        sdl2::keyboard::Keycode::Return => Some(GuiKey::ENTER),
        sdl2::keyboard::Keycode::Backspace => Some(GuiKey::BACKSPACE),
        _ => None,
    }
}
