#[allow(non_upper_case_globals)]
#[cfg(test)]
pub mod test_util {
    pub use context::Context;
    pub use element;
    pub use element::docking::split::*;
    pub use element::docking::Zone;
    pub use element::GuiVariables;
    pub use guierror;
    pub use lazy_static::*;
    pub use parser;
    pub use rect::Rect;
    pub use style::Style;
    pub use uuid::Uuid;
    pub use vector2::Vector2;

    use font;
    use ron;

    lazy_static! {
        pub static ref uuids: Vec<Uuid> = vec![
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
        ];
        // These styles are used to validate that methods/functions that use styles
        // correctly use the style's variables. Therefore all members that are not
        // colors need to be completely different
        pub static ref first_style: Style = {
            let mut s: Style = Default::default();
            s.window.title_bar_padding = 1.0;
            s.window.title_bar_title_padding = 4.0;
            s.window.border_padding = 2.0;
            s.window.border_thickness = 1.0;
            s.window.text_size = 18.0;
            s
        };
        pub static ref second_style: Style = {
            let mut s: Style = Default::default();
            s.window.title_bar_padding = 4.0;
            s.window.title_bar_title_padding = 0.0;
            s.window.border_padding = 0.0;
            s.window.border_thickness = 2.0;
            s.window.text_size = 24.0;
            s
        };
        // The area is chosen to verify that that offsets and sizes aren't mixed up between x and y
        pub static ref area: Rect = Rect::new_size(190.0, 260.0, 140.0, 365.0);
        pub static ref area_radius: Vector2 = area.size() * Vector2::new(0.5, 0.5);
        pub static ref area_center: Vector2 = area.min + *area_radius;
    }

    pub fn styled_context() -> Context {
        Context::styled_new(*first_style)
    }
    pub fn other_styled_context() -> Context {
        Context::styled_new(*second_style)
    }

    #[allow(clippy::float_cmp)]
    #[test]
    fn validate_styles_are_different() {
        assert!(
            styled_context().style != other_styled_context().style,
            "styled_context and other_styled_context return the same style"
        );
        assert_ne!(
            (*first_style).window.title_bar_padding,
            (*second_style).window.title_bar_padding,
            "title_bar_pading"
        );
        assert_ne!(
            (*first_style).window.title_bar_title_padding,
            (*second_style).window.title_bar_title_padding,
            "title_bar_title_padding"
        );
        assert_ne!(
            (*first_style).window.border_padding,
            (*second_style).window.border_padding,
            "border_padding"
        );
        assert_ne!(
            (*first_style).window.border_thickness,
            (*second_style).window.border_thickness,
            "border_thickness"
        );
        assert_ne!(
            (*first_style).window.text_size,
            (*second_style).window.text_size,
            "text_size"
        );
    }

    #[derive(Debug, Clone, Copy, PartialEq)]
    pub struct DummyElement {
        pub uuid: Uuid,
        base: element::ElementBase,
    }
    impl DummyElement {
        pub fn new() -> DummyElement {
            DummyElement::new_uuid(Uuid::new_v4())
        }
        pub fn new_uuid(uuid: Uuid) -> DummyElement {
            DummyElement {
                uuid,
                base: element::ElementBase {
                    position: Vector2::zero(),
                    size: Vector2::zero(),
                    parsed_size: element::Size {
                        width: element::Unit::Undecided,
                        height: element::Unit::Undecided,
                    },
                },
            }
        }
    }
    impl element::Element for DummyElement {
        fn new(
            _parser: &mut parser::Parser,
            _context: &mut Context,
            _value: &ron::Value,
            _base: element::ElementBase,
        ) -> Result<Self, guierror::GuiError>
        where
            Self: Sized,
        {
            Ok(DummyElement::new())
        }
        fn build(
            &mut self,
            _position: Vector2,
            _width: Option<f32>,
            _height: Option<f32>,
            _context: &mut Context,
            _font: &font::Font,
        ) {
        }
        fn draw(&mut self, _gui: &mut GuiVariables) {}
        fn box_clone(&self) -> Box<dyn element::Element> {
            Box::new(DummyElement {
                uuid: self.uuid,
                base: self.base,
            })
        }
        fn get_base(&self) -> &element::ElementBase {
            &self.base
        }
        fn get_base_mut(&mut self) -> &mut element::ElementBase {
            &mut self.base
        }
    }

    pub fn vertical_split(uuid_first: Uuid, uuid_second: Uuid) -> Zone {
        create_two_split(
            Direction::Vertical,
            Zone::Element(element::docking::DockedElement {
                title: uuid_first.to_string(),
                element: Box::new(DummyElement::new_uuid(uuid_first)),
            }),
            Zone::Element(element::docking::DockedElement {
                title: uuid_second.to_string(),
                element: Box::new(DummyElement::new_uuid(uuid_second)),
            }),
        )
    }
    pub fn horizontal_split(uuid_first: Uuid, uuid_second: Uuid) -> Zone {
        create_two_split(
            Direction::Horizontal,
            Zone::Element(element::docking::DockedElement {
                title: uuid_first.to_string(),
                element: Box::new(DummyElement::new_uuid(uuid_first)),
            }),
            Zone::Element(element::docking::DockedElement {
                title: uuid_second.to_string(),
                element: Box::new(DummyElement::new_uuid(uuid_second)),
            }),
        )
    }
    pub fn zone_element(uuid: Uuid) -> Zone {
        Zone::Element(element::docking::DockedElement {
            title: uuid.to_string(),
            element: Box::new(DummyElement::new_uuid(uuid)),
        })
    }
    pub fn unpack_split(zone: Zone) -> (Direction, Vec<Split>) {
        match zone {
            Zone::Split(direction, splits) => (direction, splits),
            _ => panic!("Zone is not a split"),
        }
    }
    pub fn assert_splits(expected: &[Split], actual: &[Split]) {
        assert_eq!(expected.len(), actual.len(), "Slices are not the same size");
        for (lhs, rhs) in expected.iter().zip(actual.iter()) {
            let size_diff = (lhs.size - rhs.size).abs();
            assert!(
                size_diff < 0.01,
                "Splits are not the same sizes. Expected {}, got {}",
                lhs.size,
                rhs.size
            );
            assert_eq!(
                lhs.zone.dummy_uuid(),
                rhs.zone.dummy_uuid(),
                "Split UUIDs are not the same. Expected"
            );
        }
        for i in 0..actual.len() {
            for j in (i + 1)..actual.len() {
                assert_ne!(
                    actual[i].zone.dummy_uuid(),
                    actual[j].zone.dummy_uuid(),
                    "Split UUIDs are equal"
                );
            }
        }
    }
    pub fn assert_zones(expected: &Zone, actual: &Zone) {
        use element::docking::Zone::*;
        match (expected, actual) {
            (Element(left_element), Element(right_element)) => {
                assert_eq!(left_element.element.dummy_uuid(), right_element.element.dummy_uuid());
            }
            (Split(left_direction, left_splits), Split(right_direction, right_splits)) => {
                assert_eq!(left_direction, right_direction, "Split directions are not the same");
                for (lhs, rhs) in left_splits.iter().zip(right_splits.iter()) {
                    let size_diff = (lhs.size - rhs.size).abs();
                    assert!(
                        size_diff < 0.01,
                        "Elements sizes are not as expected: {:?} vs {:?}",
                        lhs,
                        rhs
                    );
                    assert_zones(&lhs.zone, &rhs.zone);
                }
            }
            (Tabbed(left_index, left_tabs), Tabbed(right_index, right_tabs)) => {
                assert_eq!(left_index, right_index, "Tab indices are not the same");
                for (lhs, rhs) in left_tabs.iter().zip(right_tabs.iter()) {
                    assert_zones(&lhs, &rhs);
                }
            }
            _ => panic!(
                "Zones are not the same type. Expected\n{:?}\nbut got\n{:?}",
                expected, actual
            ),
        }
        assert_unique_uuids(actual);
    }
    pub fn assert_unique_uuids(zone: &Zone) {
        let mut map = std::collections::HashSet::<Uuid>::new();
        assert_unique_uuids_rec(zone, &mut map);
    }
    pub fn assert_unique_uuids_rec(zone: &Zone, existing_uuids: &mut std::collections::HashSet<Uuid>) {
        match zone {
            Zone::Element(boxed_element) => {
                let uuid = boxed_element.element.dummy_uuid();
                if existing_uuids.contains(&uuid) {
                    panic!("Duplicate uuid {}", uuid);
                } else {
                    existing_uuids.insert(uuid);
                }
            }
            Zone::Split(_, splits) => {
                splits
                    .iter()
                    .for_each(|split| assert_unique_uuids_rec(&split.zone, existing_uuids));
            }
            Zone::Tabbed(_, tabs) => {
                tabs.iter().for_each(|tab| assert_unique_uuids_rec(tab, existing_uuids));
            }
            Zone::Empty => {}
        }
    }

    pub trait DummyUuid {
        fn dummy_uuid(&self) -> Uuid;
    }
    impl DummyUuid for Box<dyn element::Element> {
        fn dummy_uuid(&self) -> Uuid {
            // Avert your eyes
            let box_clone = self.box_clone();
            let ptr: *const dyn element::Element = Box::into_raw(box_clone);
            let ptr: std::raw::TraitObject = unsafe { std::mem::transmute(ptr) };
            unsafe { (*(ptr.data as *mut DummyElement)).uuid }
        }
    }
    impl DummyUuid for Zone {
        fn dummy_uuid(&self) -> Uuid {
            if let Zone::Element(docked_element) = self {
                docked_element.element.dummy_uuid()
            } else {
                panic!("Zone was not element")
            }
        }
    }

    // This is a test of a test function, and is just because I don't really
    // trust what I've done in the dummy_uuid function.
    #[test]
    fn test_dummy_uuid() {
        let dummy = DummyElement::new();
        let original_uuid = dummy.uuid;
        let boxed_element: Box<dyn element::Element> = Box::new(dummy);
        assert_eq!(
            original_uuid,
            boxed_element.dummy_uuid(),
            "UUID is not the same after using dummy_uuid - fix the broken transmute usage"
        );
    }
}
