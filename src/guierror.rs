use std::convert::From;
use std::fmt;

use ron::Value;
use rusttype;

#[derive(Debug)]
pub enum GuiError {
    LoadError(String),
    ParseError(String),
    BuildError(String),
    ExpectedError(String),
}

impl fmt::Display for GuiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            GuiError::LoadError(text) => return write!(f, "{}", text),
            GuiError::ParseError(text) => return write!(f, "{}", text),
            GuiError::BuildError(text) => return write!(f, "{}", text),
            GuiError::ExpectedError(text) => return write!(f, "{}", text),
        }
    }
}

impl From<std::io::Error> for GuiError {
    fn from(err: std::io::Error) -> Self {
        GuiError::LoadError(format!("{}", err))
    }
}

impl From<ron::de::Error> for GuiError {
    fn from(err: ron::de::Error) -> Self {
        GuiError::LoadError(format!("{}", err))
    }
}

impl From<rusttype::Error> for GuiError {
    fn from(err: rusttype::Error) -> Self {
        GuiError::LoadError(format!("{}", err))
    }
}

impl GuiError {
    pub fn new_build_error(value: &Value, message: &str) -> GuiError {
        GuiError::BuildError(format!("{} while building {:?}", message, value))
    }
    pub fn new_expected_error(value: &Value, data_type: &str, member: &str) -> GuiError {
        GuiError::BuildError(format!(
            "expected field \"{}\" of type {} to exist in {:?}",
            member, data_type, value
        ))
    }
    pub fn new_expected_index_to_be_error(value: &Value, data_type: &str, index: usize) -> GuiError {
        GuiError::BuildError(format!(
            "expected index {} to be of type {} in {:?}",
            index, data_type, value
        ))
    }
    pub fn new_expected_to_contain(value: &Value, field: &str) -> GuiError {
        GuiError::BuildError(format!("expected {:?} to contain field \"{}\"", value, field))
    }
}
