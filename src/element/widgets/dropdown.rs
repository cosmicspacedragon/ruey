use context::ListProvider;
use element::utility::parse::*;
use std::slice::Iter;

use element::*;
use parsehelper::*;

use font::{Font, HorizontalOrigin, VerticalOrigin};
use ron::Value;

#[derive(Clone)]
pub struct Dropdown {
    base: ElementBase,
    selected_index: u32,
    options: Vec<String>,
    open: bool,
    provider_name: String,
    provider_index: String,
    options_provider_name: Option<String>,
    mouse_down_position: Option<Vector2>,
}

impl Element for Dropdown {
    fn new(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> Result<Self, GuiError> {
        let options_provider_name = if let Ok(options_provider) = value.expect_str("options_provider") {
            parse_vector(options_provider)
        } else {
            None
        };
        let options: Vec<String> = match value.expect_array("options") {
            Ok(options) => options
                .iter()
                .map(|option| match option {
                    Value::String(string) => string.to_string(),
                    _ => String::new(),
                })
                .filter(|option| option.is_empty())
                .collect(),
            Err(_) => Vec::new(),
        };
        if options_provider_name.is_some() && !options.is_empty() {
            return Err(GuiError::new_build_error(
                value,
                "\"options\" and \"options_provider\" are mutually exclusive",
            ));
        }
        let provider_value = value.expect_str("provider")?.to_string();
        let (provider_name, provider_index) = match parse_map(&provider_value) {
            None => {
                return Err(GuiError::new_build_error(
                    value,
                    &format!("Could not parse provider from \"{}\"", provider_value),
                ))
            }
            Some(p) => p,
        };
        let selected_index;
        match value.expect_u32("default_option") {
            Ok(val) => selected_index = val,
            Err(_) => selected_index = 0,
        };
        Ok(Dropdown {
            base,
            selected_index,
            options,
            open: false,
            provider_name,
            provider_index,
            options_provider_name,
            mouse_down_position: None,
        })
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        self.base.build(position, width, height);
        if height.is_none() {
            self.base.size.y = font.get_font_height() as f32 + Dropdown::get_padding();
        }

        let provider = if let Some(options_provider_name) = &self.options_provider_name {
            context.get_list_provider::<String>(&options_provider_name)
        } else {
            None
        };

        let mut options = if let Some(provider) = &provider {
            provider.iter()
        } else {
            self.options.iter()
        };

        if let Some(tree) = context.get_index_provider::<String>(&self.provider_name) {
            if let Some(value) = tree.get(&self.provider_index) {
                if let Some(index) = options.position(|ref val| *val == value) {
                    self.selected_index = index as u32;
                }
            }
        }
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        gui.draw_list.new_command();
        gui.draw_list.push_colored_quad(
            Rect::new_size_vector(self.base.position, self.base.size),
            gui.context.style.dropdown.background_color,
        );

        let provider = if let Some(options_provider_name) = &self.options_provider_name {
            gui.context.get_list_provider::<String>(&options_provider_name)
        } else {
            None
        };

        // This if-statement can't be moved into the other "if self.open"-block since
        // self is mutably borrowed here, and immutably borrowed just below
        if self.open && !gui.context.request_element_focus(self as *mut dyn Element) {
            panic!("Could not get focus")
        }

        let options = if let Some(provider) = &provider {
            provider.iter()
        } else {
            self.options.iter()
        };

        if let Some(position) = self.mouse_down_position {
            if self.open && self.child_list_contains(gui.font, options.len(), position.x, position.y) {
                self.selected_index = self.get_child_index(gui.font, position.y);
                self.open = false;
            } else {
                self.open = false;
            }
        }
        self.mouse_down_position = None;

        if options.len() > 0 {
            gui.draw_list.push_colored_quad(
                Rect::new_size_vector(self.base.position, Vector2::new(self.base.size.y, self.base.size.y)),
                gui.context.style.dropdown.button_color,
            );

            let index_offset = gui.draw_list.vertices.len() as u32;
            gui.draw_list.indices.push(index_offset);
            gui.draw_list.indices.push(index_offset + 1);
            gui.draw_list.indices.push(index_offset + 2);

            let arrow_offset = self.base.size.y * 0.2;
            let arrow_size = Vector2::new(
                self.base.size.y - arrow_offset * 2.0,
                self.base.size.y - arrow_offset * 2.0,
            );

            let arrow_color = gui.context.style.dropdown.arrow_color;
            if self.open {
                let arrow_position = self.base.position
                    + Vector2::new(0.0, self.base.size.y)
                    + Vector2::new(arrow_offset, -arrow_offset);
                gui.draw_list.vertices.push(Vertex::new(
                    arrow_position.x,
                    arrow_position.y,
                    1.0 / 512.0,
                    1.0 / 512.0,
                    arrow_color,
                ));
                gui.draw_list.vertices.push(Vertex::new(
                    arrow_position.x + arrow_size.x * 0.5,
                    arrow_position.y - arrow_size.y,
                    1.0 / 512.0,
                    1.0 / 512.0,
                    arrow_color,
                ));
                gui.draw_list.vertices.push(Vertex::new(
                    arrow_position.x + arrow_size.x,
                    arrow_position.y,
                    1.0 / 512.0,
                    1.0 / 512.0,
                    arrow_color,
                ));

                let background_rect = Rect::new_size_vector(
                    self.base.position + Vector2::new(0.0, self.base.size.y),
                    Vector2::new(
                        self.base.size.x,
                        gui.font.get_line_height() as f32 * options.len() as f32,
                    ),
                );
                gui.overlay_draw_list.push_clip_rect(background_rect);
                gui.overlay_draw_list.new_command();

                gui.overlay_draw_list
                    .push_colored_quad(background_rect, gui.context.style.dropdown.menu_background_color);

                let mut text_position = self.base.position + Vector2::new(Dropdown::get_padding(), self.base.size.y);
                for option in options {
                    gui.font.create_text_positioned(
                        option,
                        gui.context.style.dropdown.menu_text_color,
                        &mut gui.overlay_draw_list.vertices,
                        &mut gui.overlay_draw_list.indices,
                        text_position,
                        HorizontalOrigin::Left,
                        VerticalOrigin::Top,
                        1.0,
                    );
                    text_position.y += gui.font.get_line_height() as f32;
                }

                gui.overlay_draw_list.pop_clip_rect();
            } else {
                gui.context.release_element_focus(self as *mut dyn Element);

                let arrow_position = self.base.position + Vector2::new(arrow_offset, arrow_offset);
                gui.draw_list.vertices.push(Vertex::new(
                    arrow_position.x,
                    arrow_position.y,
                    1.0 / 512.0,
                    1.0 / 512.0,
                    arrow_color,
                ));
                gui.draw_list.vertices.push(Vertex::new(
                    arrow_position.x + arrow_size.x * 0.5,
                    arrow_position.y + arrow_size.y,
                    1.0 / 512.0,
                    1.0 / 512.0,
                    arrow_color,
                ));
                gui.draw_list.vertices.push(Vertex::new(
                    arrow_position.x + arrow_size.x,
                    arrow_position.y,
                    1.0 / 512.0,
                    1.0 / 512.0,
                    arrow_color,
                ));
            }

            let text = self.get_options(&provider).nth(self.selected_index as usize).unwrap();
            gui.font.create_text_positioned(
                &text,
                gui.context.style.dropdown.text_color,
                &mut gui.draw_list.vertices,
                &mut gui.draw_list.indices,
                self.base.position + Vector2::new(self.base.size.y + 4.0, self.base.size.y * 0.5),
                HorizontalOrigin::Left,
                VerticalOrigin::Center,
                1.0,
            );
            if let Some(provider) = gui.context.get_index_provider::<String>(&self.provider_name) {
                if let Some(value) = provider.get_mut(&self.provider_index) {
                    *value = text.to_string();
                }
            }
        }
    }

    fn mouse_down(&mut self, _context: &mut Context, x: i32, y: i32, _button: i32) -> bool {
        if self.base.contains(x, y) {
            self.open = !self.open;
            true
        } else if self.open {
            self.mouse_down_position = Some(Vector2::new(x as f32, y as f32));
            true
        } else {
            false
        }
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

impl Dropdown {
    fn child_list_contains(&self, font: &Font, option_count: usize, x: f32, y: f32) -> bool {
        let list_position = self.base.position + Vector2::new(0.0, self.base.size.y);
        let list_size = Vector2::new(self.base.size.x, option_count as f32 * font.get_line_height() as f32);

        x >= list_position.x
            && x <= list_position.x + list_size.x
            && y >= list_position.y
            && y <= list_position.y + list_size.y
    }

    fn get_child_index(&self, font: &Font, y: f32) -> u32 {
        let offset = y - (self.base.position.y + self.base.size.y);
        (offset / font.get_line_height() as f32).floor() as u32
    }

    fn get_options<'a>(&'a self, provider: &'a Option<ListProvider<String>>) -> Iter<'a, String> {
        if let Some(provider) = &provider {
            provider.iter()
        } else {
            self.options.iter()
        }
    }

    fn get_padding() -> f32 {
        4.0
    }
}
