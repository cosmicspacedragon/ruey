use element::utility::parse::*;
use element::*;
use parsehelper::*;

use font::{Font, HorizontalOrigin, VerticalOrigin};

#[derive(Clone)]
pub struct Button {
    base: ElementBase,
    text: String,
    provider_name: String,
    provider_index: String,
    mouse_down: bool,
    button_clicked: bool,
}

impl Element for Button {
    fn new(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> Result<Self, GuiError> {
        let text = value.expect_str("text")?.to_string();
        let provider_value = value.expect_str("provider")?;
        let provider_name;
        let provider_index;
        match parse_provider(provider_value) {
            Some(provider) => {
                if let Provider::Index { name, index } = provider {
                    provider_name = name;
                    provider_index = index;
                } else {
                    return Err(GuiError::new_expected_to_contain(value, "provider (index provider)"));
                }
            }
            None => {
                return Err(GuiError::new_build_error(
                    value,
                    &format!("Could not parse provider from \"{}\"", provider_value),
                ))
            }
        };
        Ok(Button {
            base,
            text,
            provider_name,
            provider_index,
            mouse_down: false,
            button_clicked: false,
        })
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        _context: &mut Context,
        font: &Font,
    ) {
        if width.is_none() || height.is_none() {
            let mut size = font.measure_str(&self.text);
            size.0 += 12;
            size.1 += 12;
            if let Some(width) = width {
                self.base.size.x = width;
            } else {
                self.base.size.x = size.0 as f32;
            }
            if let Some(height) = height {
                self.base.size.y = height;
            } else {
                self.base.size.y = size.1 as f32;
            }
        } else {
            self.base.size.x = width.unwrap();
            self.base.size.y = height.unwrap();
        }
        self.base.position = position;
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        let provider_value = if self.button_clicked {
            self.button_clicked = false;
            true
        } else {
            false
        };
        if let Some(provider) = gui.context.get_index_provider::<bool>(&self.provider_name) {
            *provider.get_mut(&self.provider_index).unwrap() = provider_value;
        }

        gui.draw_list.new_command();
        if self.mouse_down {
            gui.draw_list.push_colored_quad(
                Rect::new_size_vector(self.base.position, self.base.size),
                gui.context.style.button.background_color,
            );
        } else if self.base.contains(gui.mouse_x, gui.mouse_y) {
            gui.draw_list.push_colored_quad(
                Rect::new_size_vector(self.base.position, self.base.size),
                gui.context.style.button.background_color_hovered,
            );
        } else {
            gui.draw_list.push_colored_quad(
                Rect::new_size_vector(self.base.position, self.base.size),
                gui.context.style.button.background_color_clicked,
            );
        }
        gui.font.create_text_positioned(
            &self.text,
            gui.context.style.button.text_color,
            &mut gui.draw_list.vertices,
            &mut gui.draw_list.indices,
            self.base.position + self.base.size * 0.5,
            HorizontalOrigin::Center,
            VerticalOrigin::Center,
            1.0,
        );
    }

    fn mouse_down(&mut self, _context: &mut Context, x: i32, y: i32, _button: i32) -> bool {
        if self.base.contains(x, y) {
            self.mouse_down = true;
            true
        } else {
            false
        }
    }

    fn mouse_up(&mut self, _context: &mut Context, x: i32, y: i32, _button: i32) -> bool {
        self.mouse_down = false;
        if self.base.contains(x, y) {
            self.button_clicked = true;
            true
        } else {
            false
        }
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}
