use element::*;
use parsehelper::*;

use color::Color as rawcolor;

use std::str;
use std::str::from_utf8_unchecked;

#[derive(Clone)]
pub struct Color {
    base: ElementBase,
    color: rawcolor,
}

fn parse_u8<'a, I>(iter: &mut I) -> Result<u8, ()>
where
    I: Iterator<Item = &'a [u8]>,
{
    if let Some(str_val) = iter.next() {
        let val;
        unsafe {
            val = u8::from_str_radix(from_utf8_unchecked(str_val), 16);
        }
        match val {
            Ok(val) => return Ok(val),
            _ => return Err(()),
        }
    } else {
        return Err(());
    }
}

fn parse_color_string(value: &str) -> Result<(u8, u8, u8, u8), GuiError> {
    let mut iter = value.as_bytes().chunks(2);
    if let (Ok(r), Ok(g), Ok(b), Ok(a)) = (
        parse_u8(&mut iter),
        parse_u8(&mut iter),
        parse_u8(&mut iter),
        parse_u8(&mut iter),
    ) {
        return Ok((r, g, b, a));
    } else {
        return Err(GuiError::ParseError(format!("Couldn't parse color from \"{}\"", value)));
    }
}

impl Element for Color {
    fn new(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> Result<Self, GuiError> {
        let col = value.expect_str("color")?;
        let (r, g, b, a) = parse_color_string(col)?;
        Ok(Color {
            base,
            color: rawcolor::newb(r, g, b, a),
        })
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        _context: &mut Context,
        _font: &Font,
    ) {
        self.base.size.x = width.expect("A size was not given to color widget");
        self.base.size.y = height.expect("A size was not given to color widget");
        self.base.position = position;
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        gui.draw_list.new_command();
        gui.draw_list
            .push_colored_quad(Rect::new_size_vector(self.base.position, self.base.size), self.color);
    }

    fn get_providers(&self, _context: &mut HashSet<String>) {}
    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn allow_unsized() -> bool {
        false
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

impl Color {
    pub fn new_build(base: ElementBase, color: rawcolor) -> Self {
        Color { base, color }
    }
}
