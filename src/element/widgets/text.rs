use element::utility::parse::parse_provider;
use element::utility::parse::Provider;
use element::*;
use parsehelper::*;

use font::{Font, HorizontalOrigin, VerticalOrigin};

#[derive(Clone)]
pub struct Text {
    base: ElementBase,
    text: String,
    horizontal_origin: HorizontalOrigin,
    vertical_origin: VerticalOrigin,
    provider: Option<Provider>,
}

impl Element for Text {
    fn new(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> Result<Self, GuiError> {
        let provider;
        let text;
        match value.expect_str("text") {
            Ok(text_val) => {
                text = text_val.to_string();
                provider = None;
            }
            _ => match value.expect_str("provider") {
                Ok(provider_val) => {
                    if let Some(provider_val) = parse_provider(provider_val) {
                        text = String::new();
                        provider = Some(provider_val);
                    } else {
                        return Err(GuiError::new_build_error(
                            value,
                            &format!("Could not parse provider from {}", provider_val),
                        ));
                    }
                }
                _ => return Err(GuiError::new_expected_to_contain(value, "\"text\" or \"provider\"")),
            },
        }

        let align_str = value.optional_str("align", "top|left");
        let split_index = align_str
            .find('|')
            .ok_or_else(|| GuiError::new_build_error(value, "Missing pipe (\"|\") when parsing \"align\""))?;
        let (mut horizontal, mut vertical) = align_str.split_at(split_index);
        vertical = &vertical[1..];
        if horizontal.contains("top")
            || horizontal.contains("bottom")
            || vertical.contains("left")
            || vertical.contains("right")
        {
            std::mem::swap(&mut horizontal, &mut vertical);
        }
        let horizontal_origin = match horizontal {
            "left" => HorizontalOrigin::Left,
            "center" => HorizontalOrigin::Center,
            "right" => HorizontalOrigin::Right,
            _ => {
                return Err(GuiError::new_build_error(
                    value,
                    &format!("Could not parse horizontal origin from {}", horizontal),
                ))
            }
        };
        let vertical_origin = match vertical {
            "top" => VerticalOrigin::Top,
            "center" => VerticalOrigin::Center,
            "bottom" => VerticalOrigin::Bottom,
            _ => {
                return Err(GuiError::new_build_error(
                    value,
                    &format!("Could not parse vertical origin from {}", vertical),
                ))
            }
        };
        Ok(Text {
            base,
            text,
            horizontal_origin,
            vertical_origin,
            provider,
        })
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        self.base.position = position;
        if width.is_none() || height.is_none() {
            let mut size = font.measure_str(&self.text);
            size.0 += 12;
            size.1 += 12;
            if let Some(width) = width {
                self.base.size.x = width;
            } else {
                self.base.size.x = size.0 as f32;
            }
            if let Some(height) = height {
                self.base.size.y = height;
            } else {
                self.base.size.y = size.1 as f32;
            }
        } else {
            self.base.size = Vector2::new(width.unwrap(), height.unwrap());
        }

        if let Some(provider) = &self.provider {
            match provider {
                Provider::List(name) => {
                    if let Some(mut provider) = context.get_list_provider::<String>(&name) {
                        let text = provider.get_next();

                        if let Some(text) = text {
                            self.text = text.to_string();
                        } else {
                            self.text = "NO_VALUE".to_string();
                        }
                    }
                }
                Provider::Index { name, index } => {
                    if let Some(provider) = context.get_index_provider::<String>(&name) {
                        let text = provider.get(index);

                        if let Some(text) = text {
                            self.text = text.to_string();
                        } else {
                            self.text = "NO_VALUE".to_string();
                        }
                    }
                }
            }
        }
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        gui.draw_list.new_command();
        let mut draw_position = self.base.position;
        match self.horizontal_origin {
            HorizontalOrigin::Left => { /* No need to do anything */ }
            HorizontalOrigin::Right => draw_position.x += self.base.size.x,
            HorizontalOrigin::Center => draw_position.x += (self.base.size.x * 0.5).round(),
        }
        match self.vertical_origin {
            VerticalOrigin::Top => { /* No need to do anything */ }
            VerticalOrigin::Bottom => draw_position.y += self.base.size.y,
            VerticalOrigin::Center => draw_position.y += (self.base.size.y * 0.5).round(),
        }
        gui.font.create_text_positioned(
            &self.text,
            Color::white(),
            &mut gui.draw_list.vertices,
            &mut gui.draw_list.indices,
            draw_position,
            self.horizontal_origin,
            self.vertical_origin,
            1.0,
        );
    }

    fn get_providers(&self, context: &mut HashSet<String>) {
        if let Some(provider) = &self.provider {
            if let Provider::List(name) = provider {
                context.insert(name.to_string());
            }
        }
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}
