use element::*;
use parsehelper::*;

use element::utility::parse::{parse_provider, Provider};
use font::{Font, HorizontalOrigin, VerticalOrigin};

#[derive(Clone, Copy)]
enum Command {
    LEFT,
    RIGHT,
    ENTER,
    BACKSPACE,
    CHARACTER(char),
    NONE,
}

const MAX_COMMANDS: usize = 16;

#[derive(Clone)]
pub struct InputText {
    base: ElementBase,
    horizontal_origin: HorizontalOrigin,
    vertical_origin: VerticalOrigin,
    provider: Provider,
    editing: bool,
    index: usize,
    click_position: Option<usize>,
    commands: [Command; MAX_COMMANDS as usize],
    command_count: usize,
}

impl Element for InputText {
    fn new(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> Result<Self, GuiError> {
        let provider_value = value.expect_str("provider")?;
        let provider = match parse_provider(provider_value) {
            None => {
                return Err(GuiError::new_build_error(
                    value,
                    &format!("Could not parse provider from {}", provider_value),
                ))
            }
            Some(p) => p,
        };
        let align_str = value.optional_str("align", "top|left");
        let split_index = align_str
            .find('|')
            .ok_or_else(|| GuiError::new_build_error(value, "Missing pipe (\"|\") when parsing \"align\""))?;
        let (mut horizontal, mut vertical) = align_str.split_at(split_index);
        vertical = &vertical[1..];
        if horizontal.contains("top")
            || horizontal.contains("bottom")
            || vertical.contains("left")
            || vertical.contains("right")
        {
            std::mem::swap(&mut horizontal, &mut vertical);
        }
        let horizontal_origin = match horizontal {
            "left" => HorizontalOrigin::Left,
            "center" => HorizontalOrigin::Center,
            "right" => HorizontalOrigin::Right,
            _ => {
                return Err(GuiError::new_build_error(
                    value,
                    &format!("Could not parse horizontal origin from {}", horizontal),
                ))
            }
        };
        let vertical_origin = match vertical {
            "top" => VerticalOrigin::Top,
            "center" => VerticalOrigin::Center,
            "bottom" => VerticalOrigin::Bottom,
            _ => {
                return Err(GuiError::new_build_error(
                    value,
                    &format!("Could not parse vertical origin from {}", vertical),
                ))
            }
        };

        Ok(InputText {
            base,
            horizontal_origin,
            vertical_origin,
            provider,
            editing: false,
            index: 0,
            click_position: None,
            commands: [Command::NONE; MAX_COMMANDS],
            command_count: 0,
        })
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        _context: &mut Context,
        font: &Font,
    ) {
        self.base.build(position, width, height);
        if height.is_none() {
            self.base.size.y = font.get_font_height() as f32 + InputText::get_padding();
        }
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        gui.draw_list.new_command();
        gui.draw_list.push_colored_quad(
            Rect::new_size_vector(self.base.position, self.base.size),
            gui.context.style.input_text.background_color,
        );

        if self.editing {
            gui.context.request_keyboard_focus();
            if !gui.context.request_element_focus(self as *mut dyn Element) {
                panic!("Could not request focus");
            }
        } else {
            gui.context.release_element_focus(self as *mut dyn Element);
        }

        let mut final_text: Option<String> = None;
        match self.provider {
            Provider::List(ref name) => {
                if let Some(mut provider) = gui.context.get_list_provider::<String>(&name) {
                    if let Some(text) = provider.get_next() {
                        apply_commands(text, &mut self.index, &self.commands, &mut self.command_count);
                        final_text = Some(text.clone());
                    }
                }
            }
            Provider::Index { ref name, ref index } => {
                if let Some(provider) = gui.context.get_index_provider::<String>(&name) {
                    if let Some(text) = provider.get_mut(index) {
                        apply_commands(text, &mut self.index, &self.commands, &mut self.command_count);
                        final_text = Some(text.clone());
                    }
                }
            }
        }

        if let Some(text) = final_text {
            let start_vertex_offset = gui.draw_list.vertices.len();
            let text_position = self.get_text_position();
            gui.font.create_text_positioned(
                &text,
                gui.context.style.input_text.text_color,
                &mut gui.draw_list.vertices,
                &mut gui.draw_list.indices,
                text_position,
                self.horizontal_origin,
                self.vertical_origin,
                1.0,
            );

            let mut first_vertex_position = self.base.position;
            if let Some(first_vertex) = gui.draw_list.vertices.get(start_vertex_offset) {
                first_vertex_position = Vector2::new(first_vertex.x, first_vertex.y);
                if let Some(click_position) = self.click_position {
                    if click_position as f32 >= first_vertex.x {
                        self.index = gui
                            .font
                            .get_str_index(&text, click_position - (first_vertex.x as usize));
                    } else {
                        self.index = 0;
                    }
                    self.click_position = None;
                }
            }

            if self.editing {
                let text_center = gui.font.get_vertical_position(text_position, self.vertical_origin);

                let cursor_offset = gui.font.get_width_at_index(&text, self.index);
                let cursor_position = Vector2::new(first_vertex_position.x + cursor_offset as f32, text_center);

                if self.index > text.chars().count() {
                    self.index = text.chars().count();
                }

                gui.draw_list.push_colored_quad(
                    Rect::new_size_vector(cursor_position, Vector2::new(2.0, gui.font.get_line_height() as f32)),
                    gui.context.style.input_text.cursor_color,
                );
            }
        }
    }

    fn mouse_down(&mut self, _context: &mut Context, x: i32, y: i32, button: i32) -> bool {
        if button == 1 {
            if self.base.contains(x, y) {
                self.click_position = Some(x as usize);
                self.editing = true;
                true
            } else if self.editing {
                self.editing = false;
                true
            } else {
                false
            }
        } else {
            false
        }
    }

    fn key_down(&mut self, _context: &mut Context, key: GuiKey) -> bool {
        self.respond_to_key(key)
    }

    fn key_repeat(&mut self, _context: &mut Context, key: GuiKey) -> bool {
        self.respond_to_key(key)
    }

    fn text_input(&mut self, _context: &mut Context, text: &str) -> bool {
        if self.editing && self.command_count < MAX_COMMANDS {
            if let Some(character) = text.chars().nth(0) {
                let command = Command::CHARACTER(character);

                self.commands[self.command_count] = command;
                self.command_count += 1;
                return true;
            }
        }
        false
    }

    fn get_providers(&self, context: &mut HashSet<String>) {
        match self.provider {
            Provider::List(ref name) => context.insert(name.clone()),
            Provider::Index { ref name, .. } => context.insert(name.clone()),
        };
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

impl InputText {
    fn respond_to_key(&mut self, key: GuiKey) -> bool {
        if self.editing && self.command_count < MAX_COMMANDS {
            if let Some(command) = self.translate_key(key) {
                self.commands[self.command_count] = command;
                self.command_count += 1;
                return true;
            }
        }
        false
    }

    fn translate_key(&self, key: GuiKey) -> Option<Command> {
        match key {
            GuiKey::LEFT => Some(Command::LEFT),
            GuiKey::RIGHT => Some(Command::RIGHT),
            GuiKey::ENTER => Some(Command::ENTER),
            GuiKey::BACKSPACE => Some(Command::BACKSPACE),
            _ => None,
        }
    }

    fn get_padding() -> f32 {
        4.0
    }
    fn get_text_position(&self) -> Vector2 {
        let x_position = match self.horizontal_origin {
            HorizontalOrigin::Left => self.base.position.x + InputText::get_padding(),
            HorizontalOrigin::Right => self.base.position.x + self.base.size.x - InputText::get_padding(),
            HorizontalOrigin::Center => self.base.position.x + (self.base.size.x * 0.5).round(),
        };
        let y_position = match self.vertical_origin {
            VerticalOrigin::Top => self.base.position.y,
            VerticalOrigin::Bottom => self.base.position.y + self.base.size.y,
            VerticalOrigin::Center => self.base.position.y + (self.base.size.y * 0.5).round(),
        };
        Vector2::new(x_position, y_position)
    }
}

#[allow(clippy::needless_range_loop)] // The range is most definitively needed
fn apply_commands(text: &mut String, index: &mut usize, commands: &[Command], command_count: &mut usize) {
    for i in 0..*command_count {
        let command = commands[i];
        match command {
            Command::LEFT => {
                if *index > 0 {
                    *index -= 1;
                }
            }
            Command::RIGHT => {
                if *index < text.chars().count() {
                    *index += 1;
                }
            }
            Command::BACKSPACE => {
                if *index > 0 {
                    if let Some((i, _)) = text.char_indices().nth(*index - 1) {
                        text.remove(i);
                        *index -= 1;
                    }
                }
            }
            Command::CHARACTER(character) => {
                if let Some((i, _)) = text.char_indices().nth(*index) {
                    text.insert(i, character);
                } else {
                    text.push(character);
                }
                *index += 1;
            }
            _ => {}
        }
    }
    *command_count = 0;
}
