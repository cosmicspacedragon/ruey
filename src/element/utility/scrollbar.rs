use context::Context;
use element::GuiVariables;
use rect::Rect;

#[derive(Clone, Default)]
pub struct Scrollbar {
    area: Rect,
    max_value: f32,
    visible_values: f32,
    current_value: f32,
    mouse_down: bool,
    mouse_offset: f32,
    locked_to_bottom: bool,
}

impl Scrollbar {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn build(&mut self, area: Rect, max_value: f32, visible_values: f32) {
        self.area = area;
        self.visible_values = visible_values;
        self.max_value = max_value;
        self.locked_to_bottom = max_value <= visible_values;
    }

    pub fn draw(&mut self, gui: &mut GuiVariables) {
        gui.draw_list.new_command();
        gui.draw_list.push_colored_quad(self.area, gui.context.style.scrollbar.background_color);

        self.clamp_current_value();

        let thumb_color = if self.mouse_down {
            self.scroll(gui.mouse_y as f32);
            gui.context.style.scrollbar.thumb_color
        } else if self.thumb_contains(gui.mouse_x, gui.mouse_y) {
            gui.context.style.scrollbar.thumb_color_hovered
        } else {
            gui.context.style.scrollbar.thumb_color_clicked
        };

        let thumb_area = self.thumb_area();
        gui.draw_list.push_colored_quad(thumb_area, thumb_color);
    }

    pub fn mouse_down(&mut self, _context: &mut Context, x: i32, y: i32, _button: i32) -> bool {
        if self.thumb_contains(x, y) {
            self.mouse_down = true;
            let offset_y = self.current_value / (self.max_value - self.visible_values)
                * (self.area.size().y - self.thumb_height());
            self.mouse_offset = y as f32 - offset_y;
            true
        } else {
            false
        }
    }

    pub fn mouse_up(&mut self, _context: &mut Context, _x: i32, _y: i32, _button: i32) -> bool {
        if self.mouse_down {
            self.mouse_down = false;
            self.mouse_offset = 0.0;
            true
        } else {
            false
        }
    }

    pub fn clamp_current_value(&mut self) {
        if self.locked_to_bottom {
            self.current_value = (self.max_value - self.visible_values).max(0.0);
        } else {
            self.current_value = self.current_value.min(self.max_value - self.visible_values).max(0.0);
        }
    }

    fn scroll(&mut self, mouse_y: f32) {
        let thumb_height = self.thumb_height();
        let movable_height = self.area.size().y - thumb_height;

        let offset_y = (mouse_y - self.mouse_offset).min(movable_height).max(0.0);
        self.current_value = offset_y / movable_height * (self.max_value - self.visible_values);

        self.locked_to_bottom = self.current_value + self.visible_values >= self.max_value;
        self.current_value = self.current_value.min(self.max_value - self.visible_values).max(0.0);
    }

    pub fn get_min_range(&self) -> f32 {
        self.current_value
    }

    pub fn get_max_range(&self) -> f32 {
        self.current_value + self.visible_values
    }

    pub fn get_max_value(&self) -> f32 {
        self.max_value
    }

    pub fn get_visible_values(&self) -> f32 {
        self.visible_values
    }

    fn thumb_height(&self) -> f32 {
        (self.visible_values / self.max_value).min(1.0) * self.area.size().y
    }

    fn thumb_contains(&self, x: i32, y: i32) -> bool {
        if self.visible_values < self.max_value {
            let thumb_area = self.thumb_area();
            thumb_area.contains(x as f32, y as f32)
        } else {
            false
        }
    }

    fn thumb_area(&self) -> Rect {
        let max_value = self.max_value - self.visible_values;

        let thumb_height = self.thumb_height();
        let offset_y = self.current_value / max_value * (self.area.size().y - thumb_height);

        Rect::new_size(
            self.area.min.x,
            self.area.min.y + offset_y,
            self.area.size().x,
            thumb_height,
        )
    }
}

#[allow(non_upper_case_globals)]
#[cfg(test)]
pub mod tests {
    use element::utility::scrollbar::*;
    use testutil::test_util::*;

    const SCROLL_WIDTH: f32 = 24.0;

    fn scrollbar_area() -> Rect {
        Rect::new(
            (*area).max.x - SCROLL_WIDTH,
            (*area).min.y,
            (*area).max.x,
            (*area).max.y,
        )
    }

    fn create_scrollbar() -> Scrollbar {
        let mut temp = Scrollbar::new();
        temp.build(scrollbar_area(), 80.0, 25.0);
        temp
    }

    fn create_locked_scrollbar() -> Scrollbar {
        let mut temp = Scrollbar::new();
        temp.build(scrollbar_area(), 15.0, 25.0);
        temp
    }

    #[allow(clippy::float_cmp)]
    mod test_build {
        use super::*;

        #[test]
        fn test_unlocked_build() {
            let scrollbar = create_scrollbar();
            assert_eq!(
                scrollbar.area,
                Rect::new(
                    (*area).max.x - SCROLL_WIDTH,
                    (*area).min.y,
                    (*area).max.x,
                    (*area).max.y
                ),
                "Mispaced area"
            );
            assert_eq!(scrollbar.max_value, 80.0, "Unexpected max_value");
            assert_eq!(scrollbar.visible_values, 25.0, "Unexpected visible_values");
            assert_eq!(scrollbar.current_value, 0.0, "Unexpected current_value");
            assert_eq!(scrollbar.mouse_down, false, "Unexpected mouse_down");
            assert_eq!(scrollbar.mouse_offset, 0.0, "Unexpected mouse_offset");
            assert!(
                !scrollbar.locked_to_bottom,
                "Scrollbar should not be locked to bottom when max_values are {} and visible_values are {}",
                scrollbar.max_value, scrollbar.visible_values
            );
        }

        #[test]
        fn test_locked_build() {
            let locked_scrollbar = create_locked_scrollbar();
            assert_eq!(
                locked_scrollbar.area,
                Rect::new(
                    (*area).max.x - SCROLL_WIDTH,
                    (*area).min.y,
                    (*area).max.x,
                    (*area).max.y
                ),
                "Mispaced area"
            );
            assert_eq!(locked_scrollbar.max_value, 15.0, "Unexpected max_value");
            assert_eq!(locked_scrollbar.visible_values, 25.0, "Unexpected visible_values");
            assert_eq!(locked_scrollbar.current_value, 0.0, "Unexpected current_value");
            assert_eq!(locked_scrollbar.mouse_down, false, "Unexpected mouse_down");
            assert_eq!(locked_scrollbar.mouse_offset, 0.0, "Unexpected mouse_offset");
            assert!(
                locked_scrollbar.locked_to_bottom,
                "Scrollbar should be locked to bottom when max_values are {} and visible_values are {}",
                locked_scrollbar.max_value, locked_scrollbar.visible_values
            );
        }
    }

    mod test_mouse_down {
        use super::*;
        use approx::*;

        #[test]
        fn test_mouse_inside_thumb() {
            let mut context = styled_context();
            let mut scrollbar = create_scrollbar();
            let thumb_area = scrollbar.thumb_area();
            let thumb_center = thumb_area.min + thumb_area.size() * 0.5;
            assert!(
                scrollbar.mouse_down(&mut context, thumb_center.x as i32, thumb_center.y as i32, 1),
                "mouse_down should return true"
            );

            assert_eq!(scrollbar.mouse_down, true, "Unexpected mouse_down");
            assert!(
                abs_diff_eq!(scrollbar.mouse_offset, thumb_center.y, epsilon = 0.1),
                "Unexpected mouse_offset. Expected {}, got {}",
                thumb_center.y,
                scrollbar.mouse_offset
            );
        }

        #[allow(clippy::float_cmp)]
        #[test]
        fn test_mouse_outside_thumb() {
            let mut context = styled_context();
            let mut scrollbar = create_scrollbar();
            let thumb_area = scrollbar.thumb_area();
            assert!(
                !scrollbar.mouse_down(&mut context, thumb_area.min.x as i32, thumb_area.min.y as i32 - 1, 1),
                "mouse_down should return false"
            );
            assert!(
                !scrollbar.mouse_down(&mut context, thumb_area.max.x as i32, thumb_area.max.y as i32 + 1, 1),
                "mouse_down should return false"
            );
            assert!(
                !scrollbar.mouse_down(&mut context, thumb_area.min.x as i32 - 1, thumb_area.min.y as i32, 1),
                "mouse_down should return false"
            );
            assert!(
                !scrollbar.mouse_down(&mut context, thumb_area.max.x as i32 + 1, thumb_area.max.y as i32, 1),
                "mouse_down should return false"
            );

            assert_eq!(scrollbar.mouse_down, false, "Unexpected mouse_down");
            assert_eq!(scrollbar.mouse_offset, 0.0, "Unexpected mouse_offset");
        }
    }

    mod test_mouse_up {
        use super::*;

        #[allow(clippy::float_cmp)]
        #[test]
        fn test_mouse_inside_thumb() {
            let mut context = styled_context();
            let mut scrollbar = create_scrollbar();
            let thumb_area = scrollbar.thumb_area();
            let thumb_center = thumb_area.min + thumb_area.size() * 0.5;
            assert!(
                scrollbar.mouse_down(&mut context, thumb_center.x as i32, thumb_center.y as i32, 1),
                "mouse_down should return true"
            );
            assert!(
                scrollbar.mouse_up(&mut context, thumb_center.x as i32, thumb_center.y as i32, 1),
                "mouse_up should return true"
            );

            assert_eq!(scrollbar.mouse_down, false, "Unexpected mouse_down");
            assert_eq!(scrollbar.mouse_offset, 0.0, "Unexpected mouse_offset");
        }

        #[allow(clippy::float_cmp)]
        #[test]
        fn test_mouse_outside_thumb() {
            let mut context = styled_context();
            let mut scrollbar = create_scrollbar();
            let thumb_area = scrollbar.thumb_area();
            let thumb_center = thumb_area.min + thumb_area.size() * 0.5;
            assert!(
                !scrollbar.mouse_down(&mut context, thumb_area.min.x as i32, thumb_area.min.y as i32 - 1, 1),
                "mouse_down should return false"
            );
            assert!(
                !scrollbar.mouse_up(&mut context, thumb_center.x as i32, thumb_center.y as i32, 1),
                "mouse_up should return false"
            );

            assert_eq!(scrollbar.mouse_down, false, "Unexpected mouse_down");
            assert_eq!(scrollbar.mouse_offset, 0.0, "Unexpected mouse_offset");
        }
    }

    mod test_scroll {
        use super::*;
        use approx::*;

        #[allow(clippy::float_cmp)]
        #[test]
        fn test_scroll_down() {
            let mut context = styled_context();
            let mut scrollbar = create_scrollbar();
            let thumb_area = scrollbar.thumb_area();
            assert_eq!(
                thumb_area.min.y, scrollbar.area.min.y,
                "Unexpected scrollbar y position. Expected {}, got {}",
                scrollbar.area.min.y, thumb_area.min.y
            );
            let thumb_center = thumb_area.min + thumb_area.size() * 0.5;
            assert!(
                scrollbar.mouse_down(&mut context, thumb_center.x as i32, thumb_center.y as i32, 1),
                "mouse_down should return true"
            );

            scrollbar.scroll(area.min.y + area.size().y * 0.5);
            assert!(
                abs_diff_eq!(scrollbar.current_value, 27.5, epsilon = 0.1),
                "Unexpected current_value after scrolling. Expected {}, got {}",
                27.5,
                scrollbar.current_value
            );
        }

        #[allow(clippy::float_cmp)]
        #[test]
        fn test_scroll_clamp_down() {
            let mut context = styled_context();
            let mut scrollbar = create_scrollbar();
            let thumb_area = scrollbar.thumb_area();
            assert_eq!(
                thumb_area.min.y, scrollbar.area.min.y,
                "Unexpected scrollbar y position. Expected {}, got {}",
                scrollbar.area.min.y, thumb_area.min.y
            );
            let thumb_center = thumb_area.min + thumb_area.size() * 0.5;
            assert!(
                scrollbar.mouse_down(&mut context, thumb_center.x as i32, thumb_area.min.y as i32, 1),
                "mouse_down should return true"
            );

            scrollbar.scroll(area.max.y);
            assert_eq!(
                scrollbar.current_value,
                scrollbar.max_value - scrollbar.visible_values,
                "Unexpected current_value after scrolling"
            );

            let thumb_area = scrollbar.thumb_area();
            assert_eq!(
                thumb_area.max.y, scrollbar.area.max.y,
                "Unexpected thumb y-position after scrolling. Expected {}, got {}",
                scrollbar.area.max.y, thumb_area.max.y
            );
        }

        #[allow(clippy::float_cmp)]
        #[test]
        fn test_scroll_up() {
            let mut context = styled_context();
            let mut scrollbar = create_scrollbar();
            scrollbar.current_value = scrollbar.max_value - scrollbar.visible_values;
            let thumb_area = scrollbar.thumb_area();
            assert_eq!(
                thumb_area.max.y, scrollbar.area.max.y,
                "Unexpected scrollbar y position. Expected {}, got {}",
                scrollbar.area.max.y, thumb_area.max.y
            );

            let thumb_area = scrollbar.thumb_area();
            let thumb_center = thumb_area.min + thumb_area.size() * 0.5;
            assert!(
                scrollbar.mouse_down(&mut context, thumb_center.x as i32, thumb_center.y as i32, 1),
                "mouse_down should return true"
            );

            scrollbar.scroll(area.min.y + area.size().y * 0.5);
            assert!(
                abs_diff_eq!(scrollbar.current_value, 27.7, epsilon = 0.1),
                "Unexpected current_value after scrolling. Expected {}, got {}",
                27.7,
                scrollbar.current_value
            );
        }

        #[allow(clippy::float_cmp)]
        #[test]
        fn test_scroll_clamp_up() {
            let mut context = styled_context();
            let mut scrollbar = create_scrollbar();

            scrollbar.current_value = scrollbar.max_value - scrollbar.visible_values;
            let thumb_area = scrollbar.thumb_area();
            assert_eq!(
                thumb_area.max.y, scrollbar.area.max.y,
                "Unexpected scrollbar y position. Expected {}, got {}",
                scrollbar.area.max.y, thumb_area.max.y
            );

            let thumb_center = thumb_area.min + thumb_area.size() * 0.5;
            assert!(
                scrollbar.mouse_down(&mut context, thumb_center.x as i32, thumb_area.min.y as i32 + 1, 1),
                "mouse_down should return true"
            );

            scrollbar.scroll(area.max.y);
            assert_eq!(
                scrollbar.current_value,
                scrollbar.max_value - scrollbar.visible_values,
                "Unexpected current_value after scrolling"
            );

            let thumb_area = scrollbar.thumb_area();
            assert_eq!(
                thumb_area.max.y, scrollbar.area.max.y,
                "Unexpected thumb y-position after scrolling. Expected {}, got {}",
                scrollbar.area.max.y, thumb_area.min.y
            );
        }
    }

    mod test_clamp_current_value {
        use super::*;

        #[allow(clippy::float_cmp)]
        #[test]
        fn test_non_locked_clamp_max() {
            let mut scrollbar = create_scrollbar();
            scrollbar.current_value = scrollbar.max_value * 2.0;
            scrollbar.locked_to_bottom = false;
            scrollbar.clamp_current_value();
            assert_eq!(
                scrollbar.current_value,
                scrollbar.max_value - scrollbar.visible_values,
                "Unexpected current_value"
            );
        }

        #[allow(clippy::float_cmp)]
        #[test]
        fn test_non_locked_clamp_min() {
            let mut scrollbar = create_scrollbar();
            scrollbar.current_value = -5.0;
            scrollbar.locked_to_bottom = false;
            scrollbar.clamp_current_value();
            assert_eq!(scrollbar.current_value, 0.0, "Unexpected current_value");
        }

        #[allow(clippy::float_cmp)]
        #[test]
        fn test_locked_clamp_max() {
            let mut scrollbar = create_scrollbar();
            scrollbar.current_value = scrollbar.max_value * 2.0;
            scrollbar.locked_to_bottom = true;
            scrollbar.clamp_current_value();
            assert_eq!(
                scrollbar.current_value,
                scrollbar.max_value - scrollbar.visible_values,
                "Unexpected current_value"
            );
        }

        #[allow(clippy::float_cmp)]
        #[test]
        fn test_locked_clamp_min() {
            let mut scrollbar = create_scrollbar();
            scrollbar.current_value = -5.0;
            scrollbar.locked_to_bottom = true;
            scrollbar.clamp_current_value();
            assert_eq!(
                scrollbar.current_value,
                scrollbar.max_value - scrollbar.visible_values,
                "Unexpected current_value"
            );
        }
    }

    mod test_thumb_contains {
        use super::*;

        #[test]
        fn test_unlocked_thumb_contains() {
            let scrollbar = create_scrollbar();
            let thumb_area = scrollbar.thumb_area();
            let thumb_center = thumb_area.min + thumb_area.size() * 0.5;
            assert!(scrollbar.thumb_contains(thumb_center.x as i32, thumb_center.y as i32));
        }

        #[test]
        fn test_locked_thumb_contains() {
            let locked_scrollbar = create_locked_scrollbar();
            let thumb_area = locked_scrollbar.thumb_area();
            let thumb_center = thumb_area.min + thumb_area.size() * 0.5;
            assert!(!locked_scrollbar.thumb_contains(thumb_center.x as i32, thumb_center.y as i32));
        }
    }
}
