#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum Provider {
    List(String),
    Index { name: String, index: String },
}

pub fn parse_provider(text: &str) -> Option<Provider> {
    if text.matches(':').count() > 1 {
        return None;
    }
    let mut iter = text.split(':');
    let name = iter.next();
    let index = iter.next();
    if let Some(index) = index {
        Some(Provider::Index {
            name: name.unwrap().to_string(),
            index: index.to_string(),
        })
    } else if let Some(name) = name {
        Some(Provider::List(name.to_string()))
    } else {
        None
    }
}

pub fn parse_vector(text: &str) -> Option<String> {
    if let Some(provider) = parse_provider(text) {
        match provider {
            Provider::List(name) => return Some(name),
            _ => return None,
        }
    } else {
        None
    }
}

pub fn parse_map(text: &str) -> Option<(String, String)> {
    if let Some(provider) = parse_provider(text) {
        match provider {
            Provider::Index { name, index } => return Some((name, index)),
            _ => return None,
        }
    } else {
        None
    }
}

#[cfg(test)]
pub mod tests {
    use element::utility::parse::*;

    mod test_parse_provider {
        use super::*;

        #[test]
        fn test_parse_list_provider() {
            let provider = parse_provider("provider_name").unwrap();
            match provider {
                Provider::List(name) => assert_eq!("provider_name", &name),
                _ => panic!("\"provider_name\" was not a list provider"),
            }
        }
        #[test]
        fn test_parse_index_provider() {
            let provider = parse_provider("provider_name:provider_index").unwrap();
            match provider {
                Provider::Index { name, index } => {
                    assert_eq!("provider_name", &name);
                    assert_eq!("provider_index", &index);
                }
                _ => panic!("\"provider_name\" was not a list provider"),
            }
        }
        #[test]
        fn test_parse_multiple_colons() {
            assert!(parse_provider("a:b:c").is_none());
        }
    }

    #[test]
    fn test_parse_vector() {
        assert_eq!(parse_vector("provider_name").unwrap(), "provider_name");
        assert!(parse_vector("provider_name:provider_index").is_none());
    }

    #[test]
    fn test_parse_map() {
        assert!(parse_map("provider_name").is_none());
        assert_eq!(
            parse_map("provider_name:provider_index").unwrap(),
            ("provider_name".to_string(), "provider_index".to_string())
        );
    }
}
