use context::Context;
use element::docking::Zone;
use rect::Rect;
use vector2::Vector2;

pub fn create_tab(first: Zone, second: Zone) -> Zone {
    match first {
        Zone::Tabbed(_, mut tabs) => {
            tabs.push(second);
            Zone::Tabbed(tabs.len() - 1, tabs)
        }
        _ => match second {
            Zone::Tabbed(_, mut tabs) => {
                tabs.insert(0, first);
                Zone::Tabbed(tabs.len() - 1, tabs)
            }
            _ => Zone::Tabbed(1, vec![first, second]),
        },
    }
}

pub fn tabs_area_contains(context: &Context, area: Rect, x: f32, y: f32) -> bool {
    context.style.window.tab_tabs_area(area).contains(x, y)
}

pub struct TabRowIter {
    position: Vector2,
    width: f32,
    height: f32,
    index: usize,
    max_index: usize,
}

impl Iterator for TabRowIter {
    type Item = Rect;

    fn next(&mut self) -> Option<Rect> {
        if self.index < self.max_index {
            let return_value = Rect::new_size_vector(self.position, Vector2::new(self.width, self.height));
            self.index += 1;
            self.position.x += self.width;
            Some(return_value)
        } else {
            None
        }
    }
}

pub fn iter_tab_areas(context: &Context, area: Rect, tabs: &[Zone]) -> TabRowIter {
    TabRowIter {
        position: area.min,
        width: area.size().x / tabs.len() as f32,
        height: context.style.window.title_bar_height(),
        index: 0,
        max_index: tabs.len(),
    }
}

pub struct Tab<'a> {
    pub tab_area: Rect,
    pub element_area: Rect,
    pub zone: &'a mut Zone,
}

pub struct TabIterMut<'a> {
    area: Rect,
    index: usize,
    max_index: usize,
    iter: std::slice::IterMut<'a, Zone>,
    tabs_area: Rect,
    element_area: Rect,
}

pub trait IterTabsMut {
    fn iter_tabs_mut(&mut self, context: &Context, area: Rect) -> TabIterMut;
}

impl IterTabsMut for Vec<Zone> {
    fn iter_tabs_mut(&mut self, context: &Context, area: Rect) -> TabIterMut {
        TabIterMut {
            area,
            index: 0,
            max_index: self.len(),
            iter: self.iter_mut(),
            tabs_area: context.style.window.tab_tabs_area(area),
            element_area: context.style.window.tab_element_area(area),
        }
    }
}

impl<'a> Iterator for TabIterMut<'a> {
    type Item = Tab<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(zone) = self.iter.next() {
            self.index += 1;
            let width = self.area.size().x / self.max_index as f32;
            Some(Tab {
                tab_area: Rect::new_size(
                    self.tabs_area.min.x + width * (self.index - 1) as f32,
                    self.tabs_area.min.y,
                    width,
                    self.tabs_area.size().y,
                ),
                element_area: self.element_area,
                zone,
            })
        } else {
            None
        }
    }
}

#[allow(non_upper_case_globals)]
#[cfg(test)]
mod tests {
    use element::docking::tab::*;
    use testutil::test_util::*;

    mod test_create_tab {
        use super::*;

        #[test]
        fn test_element_element() {
            let expected = Zone::Tabbed(1, vec![zone_element(uuids[0]), zone_element(uuids[1])]);
            let tab = create_tab(zone_element(uuids[0]), zone_element(uuids[1]));
            assert_zones(&expected, &tab);
        }
        #[test]
        fn test_tab_element() {
            let expected = Zone::Tabbed(
                2,
                vec![zone_element(uuids[0]), zone_element(uuids[1]), zone_element(uuids[2])],
            );
            let tab = create_tab(
                Zone::Tabbed(1, vec![zone_element(uuids[0]), zone_element(uuids[1])]),
                zone_element(uuids[2]),
            );
            assert_zones(&expected, &tab);
        }
        #[test]
        fn test_element_tab() {
            let expected = Zone::Tabbed(
                2,
                vec![zone_element(uuids[0]), zone_element(uuids[1]), zone_element(uuids[2])],
            );
            let tab = create_tab(
                zone_element(uuids[0]),
                Zone::Tabbed(1, vec![zone_element(uuids[1]), zone_element(uuids[2])]),
            );
            assert_zones(&expected, &tab);
        }
    }

    #[test]
    fn test_iter_tab_areas() {
        let first_context = styled_context();
        let tabs = vec![zone_element(uuids[0]), zone_element(uuids[1]), zone_element(uuids[2])];

        let tab_area = first_context.style.window.tab_tabs_area(*area);
        let tab_width = tab_area.size().x / tabs.len() as f32;
        let tab_areas = vec![
            Rect::new_size(area.min.x, area.min.y, tab_width, tab_area.size().y),
            Rect::new_size(area.min.x + tab_width, area.min.y, tab_width, tab_area.size().y),
            Rect::new_size(area.min.x + tab_width * 2.0, area.min.y, tab_width, tab_area.size().y),
        ];

        for (i, tab_area) in iter_tab_areas(&first_context, *area, &tabs).enumerate() {
            assert_eq!(tab_area, tab_areas[i]);
        }
    }

    #[test]
    fn test_iter_tabs_mut() {
        let first_context = styled_context();
        let mut tabs = vec![zone_element(uuids[0]), zone_element(uuids[1]), zone_element(uuids[2])];

        let tab_area = first_context.style.window.tab_tabs_area(*area);
        let element_area = first_context.style.window.tab_element_area(*area);
        let tab_width = tab_area.size().x / tabs.len() as f32;
        let tab_areas = vec![
            Rect::new_size(area.min.x, area.min.y, tab_width, tab_area.size().y),
            Rect::new_size(area.min.x + tab_width, area.min.y, tab_width, tab_area.size().y),
            Rect::new_size(area.min.x + tab_width * 2.0, area.min.y, tab_width, tab_area.size().y),
        ];

        for (i, tab) in tabs.iter_tabs_mut(&first_context, *area).enumerate() {
            assert_eq!(tab.tab_area, tab_areas[i]);
            assert_eq!(tab.element_area, element_area);
            assert_eq!(tab.zone.dummy_uuid(), uuids[i]);
        }
    }
}
