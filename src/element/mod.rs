pub mod docking;
pub mod layouts;
pub mod utility;
pub mod widgets;

pub use color::Color;
pub use context::Context;
pub use font::Font;
pub use guierror::GuiError;
use parser::Parser;
pub use rect::Rect;
pub use ron::Value;
pub use std::collections::HashSet;
pub use vector2::Vector2;
pub use vertex::Vertex;
pub use GuiVariables;

use guikey::GuiKey;
use std::convert::TryFrom;
use std::str::FromStr;

pub enum Query {
    Command(&'static str),
    Str(&'static str, &'static str),
    Number(&'static str, i32),
    Boolean(&'static str, bool),
}

pub enum QueryResponse {
    Str(&'static str),
    Number(i32),
    Boolean(bool),
}

pub trait Element {
    fn new(parser: &mut Parser, context: &mut Context, value: &Value, base: ElementBase) -> Result<Self, GuiError>
    where
        Self: Sized;
    fn build(&mut self, position: Vector2, width: Option<f32>, height: Option<f32>, context: &mut Context, font: &Font);
    fn draw(&mut self, gui: &mut GuiVariables);
    fn get_providers(&self, _context: &mut HashSet<String>) {}
    fn box_clone(&self) -> Box<dyn Element>;
    fn key_down(&mut self, _context: &mut Context, _key: GuiKey) -> bool {
        false
    }
    fn key_up(&mut self, _context: &mut Context, _key: GuiKey) -> bool {
        false
    }
    fn key_repeat(&mut self, _context: &mut Context, _key: GuiKey) -> bool {
        false
    }
    fn mouse_down(&mut self, _context: &mut Context, _x: i32, _y: i32, _button: i32) -> bool {
        false
    }
    fn mouse_up(&mut self, _context: &mut Context, _x: i32, _y: i32, _button: i32) -> bool {
        false
    }
    fn text_input(&mut self, _context: &mut Context, _text: &str) -> bool {
        false
    }
    fn allow_unsized() -> bool
    where
        Self: Sized,
    {
        true
    }
    fn query(&mut self, _query: Query) -> Option<QueryResponse> {
        None
    }
    fn get_base(&self) -> &ElementBase;
    fn get_base_mut(&mut self) -> &mut ElementBase;
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Unit {
    Percent(f32),
    Units(f32),
    Undecided,
}

impl<'a> TryFrom<&'a str> for Unit {
    type Error = GuiError;
    fn try_from(value: &'a str) -> Result<Self, Self::Error> {
        match value.chars().last() {
            Some('%') => {
                let val = f32::from_str(&value[..value.len() - 1]);
                if let Ok(val) = val {
                    return Ok(Unit::Percent(val));
                } else {
                    return Err(GuiError::ParseError(format!(
                        "Couldn't convert {} to a percentage unit",
                        value
                    )));
                }
            }
            Some(_) => {
                let val = f32::from_str(value);
                if let Ok(val) = val {
                    return Ok(Unit::Units(val));
                } else {
                    return Err(GuiError::ParseError(format!(
                        "Couldn't convert {} to a units unit",
                        value
                    )));
                }
            }
            None => Err(GuiError::ParseError(format!("Couldn't convert {} to a Unit", value))),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Size {
    pub width: Unit,
    pub height: Unit,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct ElementBase {
    pub position: Vector2,
    pub size: Vector2,
    pub parsed_size: Size,
}

impl Clone for Box<dyn Element> {
    fn clone(&self) -> Box<dyn Element> {
        self.box_clone()
    }
}

impl ElementBase {
    pub fn build(&mut self, position: Vector2, width: Option<f32>, height: Option<f32>) {
        self.position = position;
        if let Some(width) = width {
            self.size.x = width;
        }
        if let Some(height) = height {
            self.size.y = height;
        }
    }

    pub fn contains(&self, x: i32, y: i32) -> bool {
        x >= self.position.x as i32
            && x <= (self.position.x + self.size.x) as i32
            && y >= self.position.y as i32
            && y <= (self.position.y + self.size.y) as i32
    }

    pub fn get_area(&self) -> Rect {
        Rect::new_size_vector(self.position, self.size)
    }
}
