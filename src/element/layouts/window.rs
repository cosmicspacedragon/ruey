use element::docking::{DockedElement, Zone};
use element::layouts::dockable::Dockable;
use element::*;
use font::{Font, HorizontalOrigin, VerticalOrigin};
use parsehelper::*;
use parser::Parser;

#[derive(Clone)]
pub struct Window {
    pub base: ElementBase,
    pub min_size: Vector2,
    pub max_size: Vector2,
    pub layout: Dockable,
    pub title: String,
    pub allow_resize: bool,
    pub allow_move: bool,
    pub docked: bool,
}

impl Element for Window {
    fn new(parser: &mut Parser, context: &mut Context, value: &Value, base: ElementBase) -> Result<Self, GuiError> {
        let child = parser.parse_element(context, value.expect("child")?)?;
        let child_base = ElementBase {
            position: Vector2::zero(),
            size: Vector2::zero(),
            parsed_size: Size {
                width: Unit::Undecided,
                height: Unit::Undecided,
            },
        };
        let title = value.expect_str("title")?.to_string();
        let child = Zone::Element(DockedElement {
            title: title.clone(),
            element: child,
        });
        let layout = Dockable::new_build(child_base, child);
        Ok(Window {
            base,
            min_size: value.optional_vector2("min_size", Vector2::new(24.0, 24.0)),
            max_size: value.optional_vector2("max_size", Vector2::new(::std::f32::MAX, ::std::f32::MAX)),
            title,
            layout,
            allow_resize: value.optional_bool("allow_resize", true),
            allow_move: true,
            docked: false,
        })
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        let width = if let Some(width) = width {
            width
        } else {
            match self.base.parsed_size.width {
                Unit::Units(width) => width,
                _ => 128.0,
            }
        };

        let height = if let Some(height) = height {
            height
        } else {
            match self.base.parsed_size.height {
                Unit::Units(height) => height,
                _ => 128.0,
            }
        };
        self.base.size.x = width.max(self.min_size.x).min(self.max_size.x);
        self.base.size.y = height.max(self.min_size.y).min(self.max_size.y);
        self.base.position = position;

        self.build_child(context, font);
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        self.build_child(gui.context, gui.font);

        if self.get_dockable().is_element() {
            gui.font.create_text_positioned(
                &self.title,
                Color::white(),
                &mut gui.draw_list.vertices,
                &mut gui.draw_list.indices,
                self.base.position + Vector2::new(6.0, 0.0),
                HorizontalOrigin::Left,
                VerticalOrigin::Top,
                1.0,
            );
        }

        gui.draw_list
            .push_clip_rect(Rect::new_size_vector(self.base.position, self.base.size));

        gui.draw_list.new_command();
        self.layout.draw(gui);
        gui.draw_list.pop_clip_rect();
    }

    fn get_providers(&self, context: &mut HashSet<String>) {
        self.layout.get_providers(context);
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn mouse_down(&mut self, context: &mut Context, x: i32, y: i32, button: i32) -> bool {
        self.layout.mouse_down(context, x, y, button)
    }

    fn mouse_up(&mut self, context: &mut Context, x: i32, y: i32, button: i32) -> bool {
        self.layout.mouse_up(context, x, y, button)
    }

    fn key_down(&mut self, context: &mut Context, key: GuiKey) -> bool {
        self.layout.key_down(context, key)
    }

    fn key_up(&mut self, context: &mut Context, key: GuiKey) -> bool {
        self.layout.key_up(context, key)
    }

    fn key_repeat(&mut self, context: &mut Context, key: GuiKey) -> bool {
        self.layout.key_repeat(context, key)
    }

    fn text_input(&mut self, context: &mut Context, text: &str) -> bool {
        self.layout.text_input(context, text)
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

impl Window {
    pub fn rebuild(&mut self, context: &mut Context, font: &Font) {
        self.build_child(context, font);
    }

    pub fn get_undocked_element(&mut self, context: &Context, mouse_x: i32, mouse_y: i32) -> Option<Zone> {
        let undocked_element = self.get_dockable().get_undocked_element(context, mouse_x, mouse_y);
        if undocked_element.is_some() {
            if let Some(title) = self.get_dockable().get_title() {
                self.title = title;
            }
        }
        undocked_element
    }

    pub fn dock(&mut self, context: &Context, dockable: &mut Dockable, x_pos: i32, y_pos: i32) -> bool {
        self.layout.dock(context, dockable, x_pos, y_pos)
    }

    fn build_child(&mut self, context: &mut Context, font: &Font) {
        self.layout.build(
            self.base.position,
            Some(self.base.size.x),
            Some(self.base.size.y),
            context,
            font,
        );
    }

    pub fn allow_move(&self) -> bool {
        self.allow_move
    }

    pub fn allow_resize(&self) -> bool {
        self.allow_resize
    }

    pub fn docked(&self) -> bool {
        self.docked
    }

    pub fn draw_docking_ui(&mut self, gui: &mut GuiVariables) {
        self.layout.draw_docking_ui(gui);
    }

    pub fn get_dockable(&mut self) -> &mut Dockable {
        &mut self.layout
    }
}
