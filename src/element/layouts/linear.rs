use element::*;
use parsehelper::*;
use parser::Parser;

#[derive(Clone)]
pub enum Direction {
    Vertical,
    Horizontal,
}

#[derive(Clone)]
pub struct Linear {
    base: ElementBase,
    direction: Direction,
    children: Vec<Box<dyn Element>>,
    element_padding: i32,
}

pub struct BuildArgs {
    pub base: ElementBase,
    pub direction: Direction,
    pub children: Vec<Box<dyn Element>>,
    pub element_padding: Option<i32>,
}

impl Element for Linear {
    fn new(parser: &mut Parser, context: &mut Context, value: &Value, base: ElementBase) -> Result<Self, GuiError> {
        let direction;
        match value.expect_str("direction")? {
            "vertical" | "vert" | "v" => direction = Direction::Vertical,
            "horizontal" | "hori" | "h" => direction = Direction::Horizontal,
            val => {
                return Err(GuiError::ParseError(format!(
                    "Expected vertical, horizontal, or a variation. Got {}",
                    val
                )))
            }
        }

        let arr = value.expect_array("children")?;
        let mut children = Vec::<Box<dyn Element>>::with_capacity(arr.len());
        for element in arr {
            children.push(parser.parse_element(context, element)?);
        }

        Ok(Linear {
            base,
            direction,
            children,
            element_padding: value.optional_i32("element_padding", 12),
        })
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        self.base.build(position, width, height);
        let mut max_width = 0.0f32;
        let mut max_height = 0.0f32;
        let mut child_pos = self.base.position;
        for child in &mut self.children {
            let child_width;
            match child.get_base().parsed_size.width {
                Unit::Percent(percent) => child_width = Some(self.base.size.x * percent * 0.01),
                Unit::Units(unit) => child_width = Some(unit),
                Unit::Undecided => child_width = None,
            }
            let child_height;
            match child.get_base().parsed_size.height {
                Unit::Percent(percent) => child_height = Some(self.base.size.y * percent * 0.01),
                Unit::Units(unit) => child_height = Some(unit),
                Unit::Undecided => child_height = None,
            }
            child.build(child_pos, child_width, child_height, context, font);
            match self.direction {
                Direction::Horizontal => child_pos.x += child.get_base().size.x + self.element_padding as f32,
                Direction::Vertical => child_pos.y += child.get_base().size.y + self.element_padding as f32,
            }
            max_width = max_width.max(child.get_base().size.x);
            max_height = max_height.max(child.get_base().size.y);
        }

        if width.is_none() {
            self.base.size.x = max_width;
        }
        if height.is_none() {
            self.base.size.y = max_height;
        }
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        for child in &mut self.children {
            child.draw(gui);
        }
    }

    fn get_providers(&self, context: &mut HashSet<String>) {
        for element in &self.children {
            element.get_providers(context);
        }
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn mouse_down(&mut self, context: &mut Context, x: i32, y: i32, button: i32) -> bool {
        for child in &mut self.children {
            if child.mouse_down(context, x, y, button) {
                return true;
            }
        }
        false
    }

    fn mouse_up(&mut self, context: &mut Context, x: i32, y: i32, button: i32) -> bool {
        for child in &mut self.children {
            if child.mouse_up(context, x, y, button) {
                return true;
            }
        }
        false
    }

    fn key_down(&mut self, context: &mut Context, key: GuiKey) -> bool {
        for child in &mut self.children {
            if child.key_down(context, key) {
                return true;
            }
        }
        false
    }

    fn key_up(&mut self, context: &mut Context, key: GuiKey) -> bool {
        for child in &mut self.children {
            if child.key_up(context, key) {
                return true;
            }
        }
        false
    }

    fn key_repeat(&mut self, context: &mut Context, key: GuiKey) -> bool {
        for child in &mut self.children {
            if child.key_repeat(context, key) {
                return true;
            }
        }
        false
    }

    fn text_input(&mut self, context: &mut Context, text: &str) -> bool {
        for child in &mut self.children {
            if child.text_input(context, text) {
                return true;
            }
        }
        false
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

impl Linear {
    pub fn new_build(args: BuildArgs) -> Self {
        Linear {
            base: args.base,
            direction: args.direction,
            children: args.children,
            element_padding: args.element_padding.unwrap_or(12),
        }
    }

    pub fn take_child(&mut self, i: usize) -> Box<dyn Element> {
        self.children.remove(i)
    }

    pub fn push(&mut self, child: Box<dyn Element>) {
        self.children.push(child);
    }

    pub fn set_direction(&mut self, direction: Direction) {
        self.direction = direction;
    }
}
