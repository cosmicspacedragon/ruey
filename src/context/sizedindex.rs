use std::slice::{Iter, IterMut};

#[allow(clippy::len_without_is_empty)]
pub trait SizedIndex<T> {
    fn get(&mut self, index: usize) -> Option<&mut T>;
    fn len(&self) -> usize;
    fn iter(&self) -> Iter<T>;
    fn iter_mut(&mut self) -> IterMut<T>;
}

impl<T> SizedIndex<T> for Vec<T> {
    fn get(&mut self, index: usize) -> Option<&mut T> {
        self.get_mut(index)
    }
    fn len(&self) -> usize {
        self.len()
    }

    fn iter(&self) -> Iter<T> {
        self.as_slice().iter()
    }

    fn iter_mut(&mut self) -> IterMut<T> {
        self.as_mut_slice().iter_mut()
    }
}

#[cfg(test)]
mod tests {
    use context::sizedindex::SizedIndex;

    mod test_vec_impl {
        use super::*;
        #[test]
        fn test_get_and_len() {
            let mut vec = vec![1, 2, 3, 4];
            assert_eq!(*SizedIndex::get(&mut vec, 0).unwrap(), 1);
            assert_eq!(*SizedIndex::get(&mut vec, 1).unwrap(), 2);
            assert_eq!(*SizedIndex::get(&mut vec, 2).unwrap(), 3);
            assert_eq!(*SizedIndex::get(&mut vec, 3).unwrap(), 4);
            assert_eq!(SizedIndex::len(&vec), 4);

            assert!(SizedIndex::get(&mut vec, 123).is_none());
        }
        #[test]
        fn test_iter() {
            let mut vec = vec![1, 2, 3, 4];
            for (i, val) in SizedIndex::iter(&vec).enumerate() {
                assert_eq!(i + 1 as usize, *val);
            }
            for (i, val) in SizedIndex::iter_mut(&mut vec).enumerate() {
                *val = 4 - i;
                assert_eq!((4 - i) as usize, *val);
            }
        }
    }
}
