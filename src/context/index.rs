use std::collections::BTreeMap;

pub trait Index<V> {
    fn get(&self, index: &str) -> Option<&V>;
    fn get_mut(&mut self, index: &str) -> Option<&mut V>;
}

impl<V> Index<V> for BTreeMap<String, V> {
    fn get(&self, index: &str) -> Option<&V> {
        self.get(index)
    }
    fn get_mut(&mut self, index: &str) -> Option<&mut V> {
        self.get_mut(index)
    }
}

#[cfg(test)]
mod tests {
    use context::index::Index;

    mod test_btreemap_impl {
        use super::*;
        use std::collections::BTreeMap;
        #[test]
        fn test_iter() {
            let mut map: BTreeMap<String, String> = BTreeMap::new();
            map.insert("first".to_string(), "1".to_string());
            map.insert("second".to_string(), "2".to_string());
            map.insert("third".to_string(), "3".to_string());

            assert_eq!(Index::get(&map, "first").unwrap(), "1");
            assert_eq!(Index::get(&map, "second").unwrap(), "2");
            assert_eq!(Index::get(&map, "third").unwrap(), "3");

            *Index::get_mut(&mut map, "first").unwrap() = "one".to_string();
            *Index::get_mut(&mut map, "second").unwrap() = "two".to_string();
            *Index::get_mut(&mut map, "third").unwrap() = "three".to_string();

            assert_eq!(Index::get(&map, "first").unwrap(), "one");
            assert_eq!(Index::get(&map, "second").unwrap(), "two");
            assert_eq!(Index::get(&map, "third").unwrap(), "three");

            assert!(Index::get(&map, "Nothing").is_none());
            assert!(Index::get_mut(&mut map, "Nothing").is_none());
        }
    }
}
