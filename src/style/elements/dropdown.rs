use color::Color;
use serde::Deserialize;

#[derive(Copy, Clone, Deserialize, Debug, PartialEq)]
#[cfg_attr(test, serde(default), derive(Default))]
pub struct Dropdown {
    pub text_color: Color,
    pub background_color: Color,
    pub button_color: Color,
    pub arrow_color: Color,
    pub menu_background_color: Color,
    pub menu_text_color: Color,
}
