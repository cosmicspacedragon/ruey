use serde::Deserialize;

use color::Color;

#[derive(Copy, Clone, Deserialize, Debug, PartialEq)]
#[cfg_attr(test, serde(default), derive(Default))]
pub struct InputText {
    pub text_color: Color,
    pub background_color: Color,
    pub cursor_color: Color,
}
