use serde::Deserialize;

use color::Color;

#[derive(Copy, Clone, Deserialize, Debug, PartialEq)]
#[cfg_attr(test, serde(default), derive(Default))]
pub struct Scrollbar {
    pub background_color: Color,
    pub thumb_color: Color,
    pub thumb_color_hovered: Color,
    pub thumb_color_clicked: Color,
}
