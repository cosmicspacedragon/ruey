use element::Unit;
use guierror::GuiError;
use ron::Value;
use vector2::Vector2;

pub trait ExpectedValue {
    fn expect(&self, value: &str) -> Result<&Value, GuiError>;
    fn expect_i32(&self, value: &str) -> Result<i32, GuiError>;
    fn expect_u32(&self, value: &str) -> Result<u32, GuiError>;
    fn expect_bool(&self, value: &str) -> Result<bool, GuiError>;
    fn expect_str(&self, value: &str) -> Result<&str, GuiError>;
    fn expect_array(&self, value: &str) -> Result<&Vec<Value>, GuiError>;
    fn expect_vector2(&self, value: &str) -> Result<Vector2, GuiError>;
    fn expect_unit(&self, value: &str) -> Result<Unit, GuiError>;
}
pub trait OptionalValue {
    fn optional_str(&self, value: &str, default: &str) -> String;
    fn optional_i32(&self, value: &str, default: i32) -> i32;
    fn optional_u32(&self, value: &str, default: u32) -> u32;
    fn optional_bool(&self, value: &str, default: bool) -> bool;
    fn optional_vector2(&self, value: &str, default: Vector2) -> Vector2;
    fn optional_unit(&self, value: &str, default: Unit) -> Unit;
}

impl ExpectedValue for Value {
    fn expect(&self, value: &str) -> Result<&Value, GuiError> {
        match self {
            Value::Map(map) => {
                if let Some(value) = map.get(&Value::String(value.to_string())) {
                    Ok(value)
                } else {
                    Err(GuiError::new_expected_to_contain(self, value))
                }
            }
            _ => Err(GuiError::new_expected_to_contain(self, value)),
        }
    }

    fn expect_i32(&self, value: &str) -> Result<i32, GuiError> {
        match self.expect(value)? {
            Value::Number(number) => Ok(number.get() as i32),
            _ => Err(GuiError::new_expected_error(self, "i32", value)),
        }
    }

    fn expect_u32(&self, value: &str) -> Result<u32, GuiError> {
        match self.expect(value)? {
            Value::Number(number) => Ok(number.get() as u32),
            _ => Err(GuiError::new_expected_error(self, "u32", value)),
        }
    }

    fn expect_bool(&self, value: &str) -> Result<bool, GuiError> {
        match self.expect(value)? {
            Value::Bool(boolean) => Ok(*boolean),
            _ => Err(GuiError::new_expected_error(self, "u32", value)),
        }
    }

    fn expect_str(&self, value: &str) -> Result<&str, GuiError> {
        match self.expect(value)? {
            Value::String(string) => Ok(string),
            _ => Err(GuiError::new_expected_error(self, "string", value)),
        }
    }

    fn expect_array(&self, value: &str) -> Result<&Vec<Value>, GuiError> {
        match self.expect(value)? {
            Value::Seq(array) => Ok(array),
            _ => Err(GuiError::new_expected_error(self, "array", value)),
        }
    }

    fn expect_vector2(&self, value: &str) -> Result<Vector2, GuiError> {
        match self.expect(value)? {
            Value::Seq(array) => {
                if array.len() != 2 {
                    return Err(GuiError::new_expected_error(self, "vector2", value));
                }
                let x = match array.get(0).unwrap() {
                    Value::Number(number) => number.get(),
                    _ => return Err(GuiError::new_expected_index_to_be_error(self, "number", 0)),
                };
                let y = match array.get(1).unwrap() {
                    Value::Number(number) => number.get(),
                    _ => return Err(GuiError::new_expected_index_to_be_error(self, "number", 1)),
                };
                Ok(Vector2::new(x as f32, y as f32))
            }
            _ => Err(GuiError::new_expected_error(self, "array", value)),
        }
    }

    fn expect_unit(&self, value: &str) -> Result<Unit, GuiError> {
        match self.expect(value)? {
            Value::Number(number) => {
                let number = number.get();
                if number.fract() == 0.0 {
                    Ok(Unit::Units(number as f32))
                } else {
                    Ok(Unit::Percent(number as f32))
                }
            }
            _ => Err(GuiError::new_expected_error(self, "number", value)),
        }
    }
}

impl OptionalValue for Value {
    fn optional_str(&self, value: &str, default: &str) -> String {
        match self.expect_str(value) {
            Ok(value) => value.to_string(),
            _ => default.to_string(),
        }
    }

    fn optional_i32(&self, value: &str, default: i32) -> i32 {
        match self.expect_i32(value) {
            Ok(value) => value,
            _ => default,
        }
    }

    fn optional_u32(&self, value: &str, default: u32) -> u32 {
        match self.expect_u32(value) {
            Ok(value) => value,
            _ => default,
        }
    }

    fn optional_bool(&self, value: &str, default: bool) -> bool {
        match self.expect_bool(value) {
            Ok(value) => value,
            _ => default,
        }
    }

    fn optional_vector2(&self, value: &str, default: Vector2) -> Vector2 {
        match self.expect_vector2(value) {
            Ok(value) => value,
            _ => default,
        }
    }

    fn optional_unit(&self, value: &str, default: Unit) -> Unit {
        match self.expect_unit(value) {
            Ok(value) => value,
            _ => default,
        }
    }
}
