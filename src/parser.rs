use context::Context;
use element::{
    layouts::{linear::Linear, list::List, window::Window},
    widgets::{button::Button, color::Color, dropdown::Dropdown, inputtext::InputText, text::Text},
    Element, ElementBase, Size, Unit,
};
use guierror::GuiError;
use parsehelper::ExpectedValue;
use ron::Value;
use std::convert::TryFrom;
use vector2::Vector2;
use windowmanager::WindowManager;

pub struct Parser<'a> {
    parsing_window: bool,
    window_manager: &'a mut WindowManager,
}

impl<'a> Parser<'a> {
    pub fn new(window_manager: &'a mut WindowManager) -> Self {
        Parser {
            parsing_window: false,
            window_manager,
        }
    }
    fn parse_window(&mut self, context: &mut Context, json: &Value) -> Result<(), GuiError> {
        let element_type = json.expect_str("type")?;
        if element_type != "window" {
            return Err(GuiError::ParseError(format!(
                "All elements must be contained in a window, but this isn't: {:?}",
                json
            )));
        }
        if self.parsing_window {
            return Err(GuiError::new_build_error(json, "Nested window layouts are not allowed"));
        }
        self.parsing_window = true;
        let window = Parser::create_element::<Window>(self, context, json)?;
        self.parsing_window = false;
        self.window_manager.add_window(window);
        Ok(())
    }
    pub fn parse_element(&mut self, context: &mut Context, json: &Value) -> Result<Box<dyn Element>, GuiError> {
        let element_type = json.expect_str("type")?;
        match element_type {
            "linear" => Parser::create_boxed_element::<Linear>(self, context, json),
            "color" => Parser::create_boxed_element::<Color>(self, context, json),
            "text" => Parser::create_boxed_element::<Text>(self, context, json),
            "inputtext" => Parser::create_boxed_element::<InputText>(self, context, json),
            "list" => Parser::create_boxed_element::<List>(self, context, json),
            "button" => Parser::create_boxed_element::<Button>(self, context, json),
            "dropdown" => Parser::create_boxed_element::<Dropdown>(self, context, json),
            "window" => Err(GuiError::new_build_error(json, "Nested window layouts are not allowed")),
            _ => Err(GuiError::ParseError(format!(
                "No element \"{}\" exists, parsing {:?}",
                element_type, json
            ))),
        }
    }
    pub fn parse_root(&mut self, context: &mut Context, value: &Value) -> Result<(), GuiError> {
        match value {
            Value::Seq(vec) => {
                for element in vec {
                    self.parse_window(context, element)?;
                }
                Ok(())
            }
            _ => panic!("No"),
        }
    }
    pub fn create_boxed_element<T: Element + 'static>(
        parser: &mut Parser,
        context: &mut Context,
        value: &Value,
    ) -> Result<Box<dyn Element>, GuiError> {
        Ok(Box::new(Parser::create_element::<T>(parser, context, value)?))
    }
    pub fn create_element<T: Element + 'static>(
        parser: &mut Parser,
        context: &mut Context,
        value: &Value,
    ) -> Result<T, GuiError> {
        let width = match value.expect_unit("width") {
            Ok(unit) => unit,
            _ => match value.expect_str("width") {
                Ok(width_str) => Unit::try_from(width_str)?,
                _ => {
                    if T::allow_unsized() {
                        Unit::Undecided
                    } else {
                        return Err(GuiError::new_expected_error(value, "unit (float or int)", "width"));
                    }
                }
            },
        };
        let height = match value.expect_unit("height") {
            Ok(unit) => unit,
            _ => match value.expect_str("height") {
                Ok(width_str) => Unit::try_from(width_str)?,
                _ => {
                    if T::allow_unsized() {
                        Unit::Undecided
                    } else {
                        return Err(GuiError::new_expected_error(value, "unit (float or int)", "height"));
                    }
                }
            },
        };
        let parsed_size = Size { width, height };
        let base = ElementBase {
            position: Vector2::zero(),
            size: Vector2::zero(),
            parsed_size,
        };

        Ok(T::new(parser, context, value, base)?)
    }
}
