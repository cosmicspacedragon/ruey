use context::Context;
use element::{layouts::dockable::Dockable, layouts::window::Window, Element, ElementBase, Size, Unit, Vertex};
use font::Font;
use guikey::GuiKey;
use rect::Rect;
use std::vec::Vec;
use vector2::Vector2;
use GuiVariables;

pub const TITLE_BAR_PADDING: f32 = 8.0;
pub const BORDER_PADDING: f32 = 1.0;
pub const CORNER_RESIZE_SIZE: f32 = 24.0;

#[derive(Clone, PartialEq)]
enum DragState {
    MOVE,
    RESIZE,
    DOCKING,
    DOCKED(usize),
}

#[derive(Default)]
pub struct WindowManager {
    windows: Vec<Window>,
    title_bar_height: i32,
    drag_state: Option<DragState>,
    drag_mouse_offset: Option<Vector2>,
    mouse_down: bool,
}

impl WindowManager {
    pub fn add_window(&mut self, window: Window) {
        self.windows.push(window);
    }

    pub fn build(&mut self, area: Rect, context: &mut Context, font: &mut Font) {
        let mut window_position = area.min;
        for window in self.windows.iter_mut().rev() {
            window.build(window_position, None, None, context, font);
            window_position.x += 32.0;
            window_position.y += 32.0;
        }
    }

    pub fn key_down(&mut self, context: &mut Context, key: GuiKey) {
        if let Some(window) = self.windows.get_mut(0) {
            window.key_down(context, key);
        }
    }

    pub fn key_repeat(&mut self, context: &mut Context, key: GuiKey) {
        if let Some(window) = self.windows.get_mut(0) {
            window.key_repeat(context, key);
        }
    }

    pub fn key_up(&mut self, context: &mut Context, key: GuiKey) {
        if let Some(window) = self.windows.get_mut(0) {
            window.key_up(context, key);
        }
    }

    pub fn text_input(&mut self, context: &mut Context, text: &str) {
        if let Some(window) = self.windows.get_mut(0) {
            window.text_input(context, text);
        }
    }

    pub fn mouse_down(&mut self, context: &mut Context, pos_x: i32, pos_y: i32, button: i32) {
        self.mouse_down = true;
        if !self.windows.is_empty() {
            let mut clicked_index = None;
            let mut switch_selected = true;
            for (i, window) in self.windows.iter().enumerate() {
                if window.get_base().get_area().contains(pos_x as f32, pos_y as f32) {
                    if window.docked() {
                        switch_selected = false;
                    }
                    clicked_index = Some(i);
                    break;
                }
            }
            if let Some(i) = clicked_index {
                let window = if switch_selected {
                    let window = self.windows.remove(i);
                    self.windows.insert(0, window);
                    self.windows.get_mut(0)
                } else {
                    self.windows.get_mut(i)
                }
                .unwrap();

                if button == 1
                    && window.allow_resize()
                    && WindowManager::bottom_right_corner_contains(window.get_base(), pos_x, pos_y)
                {
                    self.drag_state = Some(DragState::RESIZE);
                    self.drag_mouse_offset = Some(Vector2::new(
                        window.get_base().position.x + window.get_base().size.x - pos_x as f32,
                        window.get_base().position.y + window.get_base().size.y - pos_y as f32,
                    ));
                } else {
                    let event_handled = window.mouse_down(context, pos_x, pos_y, button);

                    let window = self.windows.get_mut(0).unwrap();
                    if !event_handled && window.allow_move() {
                        let window_base = window.get_base();
                        self.drag_mouse_offset = Some(Vector2::new(
                            pos_x as f32 - window_base.position.x,
                            pos_y as f32 - window_base.position.y,
                        ));
                        self.drag_state = if button == 1 {
                            Some(DragState::MOVE)
                        } else {
                            Some(DragState::DOCKING)
                        };
                    }
                }
            }
        }
    }

    pub fn mouse_up(&mut self, context: &mut Context, pos_x: i32, pos_y: i32, button: i32) {
        self.mouse_down = false;
        if self.windows.is_empty() {
            self.drag_state = None;
            return;
        }
        let window = self.windows.get_mut(0).unwrap();
        let mut reset_drag_state = true;
        if Some(DragState::DOCKING) == self.drag_state {
            for i in (1..self.windows.len()).rev() {
                let window = self.windows.get_mut(i).unwrap();
                if window.get_base().contains(pos_x, pos_y) {
                    reset_drag_state = false;
                    self.drag_state = Some(DragState::DOCKED(i));
                }
            }
        } else {
            window.mouse_up(context, pos_x, pos_y, button);
        }
        if reset_drag_state {
            self.drag_state = None;
        }
    }

    pub fn draw(&mut self, gui: &mut GuiVariables) {
        self.title_bar_height = gui.font.get_line_height() + (TITLE_BAR_PADDING as i32);

        if let Some(ref drag_state) = self.drag_state {
            let offset = self
                .drag_mouse_offset
                .expect("drag_mouse_offset not set when drag_state was");
            match drag_state {
                DragState::MOVE => {
                    let window: &mut Window;
                    unsafe {
                        window = self.windows.get_unchecked_mut(0);
                    }
                    window.get_base_mut().position = Vector2::new(gui.mouse_x as f32, gui.mouse_y as f32) - offset;

                    let position = window.get_base().position;
                    let size = window.get_base().size;

                    (*window).build(position, Some(size.x), Some(size.y), gui.context, gui.font);
                }
                DragState::RESIZE => {
                    let window: &mut Window;
                    unsafe {
                        window = self.windows.get_unchecked_mut(0);
                    }
                    let new_size = Vector2::new(
                        gui.mouse_x as f32 - window.get_base().position.x,
                        gui.mouse_y as f32 - window.get_base().position.y,
                    ) + offset;

                    let position = window.get_base().position;

                    (*window).build(position, Some(new_size.x), Some(new_size.y), gui.context, gui.font);
                }
                DragState::DOCKING => {
                    let window: &mut Window;
                    unsafe {
                        window = self.windows.get_unchecked_mut(0);
                    }
                    window.get_base_mut().position = Vector2::new(gui.mouse_x as f32, gui.mouse_y as f32) - offset;

                    let position = window.get_base().position;
                    let size = window.get_base().size;

                    (*window).build(position, Some(size.x), Some(size.y), gui.context, gui.font);
                }
                DragState::DOCKED(target_index) => {
                    let mut source_window = self.windows.remove(0);
                    unsafe {
                        // The first window was removed, therefore `target_index - 1`
                        if !self.windows.get_unchecked_mut(target_index - 1).dock(
                            gui.context,
                            source_window.get_dockable(),
                            gui.mouse_x,
                            gui.mouse_y,
                        ) {
                            self.windows.insert(0, source_window);
                        } else {
                            self.windows.get_unchecked_mut(target_index - 1).title = "".to_string();
                        }
                    }
                    self.drag_state = None;
                }
            }
        }

        unsafe {
            if let Some(DragState::DOCKING) = self.drag_state {
                let mut hovered_window = None;
                for window in self.windows.iter_mut().skip(1).rev() {
                    window.draw(gui);
                }
                self.windows.get_unchecked_mut(0).draw(gui);

                for window in self.windows.iter_mut().skip(1).rev() {
                    if window.get_base().contains(gui.mouse_x, gui.mouse_y) {
                        hovered_window = Some(window);
                    }
                }
                if let Some(hovered_window) = hovered_window {
                    let clip_rect =
                        Rect::new_size_vector(hovered_window.get_base().position, hovered_window.get_base().size);
                    gui.draw_list.push_clip_rect(clip_rect);
                    gui.draw_list.new_command();
                    hovered_window.draw_docking_ui(gui);
                    gui.draw_list.pop_clip_rect();
                }
            } else {
                for mut i in (0..self.windows.len()).rev() {
                    if let Some(undocked_element) =
                        self.windows
                            .get_mut(i)
                            .unwrap()
                            .get_undocked_element(gui.context, gui.mouse_x, gui.mouse_y)
                    {
                        let child_base = ElementBase {
                            position: Vector2::zero(),
                            size: Vector2::zero(),
                            parsed_size: Size {
                                width: Unit::Undecided,
                                height: Unit::Undecided,
                            },
                        };
                        let layout = Dockable::new_build(child_base, undocked_element);

                        let base = ElementBase {
                            position: Vector2::new(gui.mouse_x as f32, gui.mouse_y as f32),
                            size: Vector2::new(256.0, 256.0),
                            parsed_size: Size {
                                width: Unit::Undecided,
                                height: Unit::Undecided,
                            },
                        };
                        let new_window = Window {
                            base,
                            min_size: Vector2::new(128.0, 128.0),
                            max_size: Vector2::new(512.0, 512.0),
                            title: "Undocked".to_string(),
                            layout,
                            allow_resize: true,
                            allow_move: true,
                            docked: false,
                        };
                        i += 1;
                        self.windows.insert(0, new_window);

                        if self.mouse_down {
                            self.drag_state = Some(DragState::DOCKING);
                            self.drag_mouse_offset = Some(Vector2::new(
                                self.title_bar_height as f32 * 0.5,
                                self.title_bar_height as f32 * 0.5,
                            ));
                        }
                    }
                    let window = self.windows.get_mut(i).unwrap();
                    window.draw(gui);

                    if window.allow_resize() && window.get_base().contains(gui.mouse_x, gui.mouse_y) {
                        gui.draw_list.new_command();
                        WindowManager::draw_corners(window.get_base(), gui);
                    }
                }
            };
        }
    }

    fn bottom_right_corner_contains(window_base: &ElementBase, mouse_x: i32, mouse_y: i32) -> bool {
        let bottom_right = window_base.position + Vector2::new(window_base.size.x, window_base.size.y);
        let distance = (bottom_right.x - mouse_x as f32) + (bottom_right.y - mouse_y as f32);
        distance >= 0.0 && distance <= CORNER_RESIZE_SIZE
    }

    fn draw_corners(window_base: &ElementBase, gui: &mut GuiVariables) {
        /*let top_left = base.position;
        if (gui.mouse_x as f32 - top_left.x) + (gui.mouse_y as f32 - top_left.y) < CORNER_RESIZE_SIZE {
            self.draw_triangle(top_left, top_left + Vector2::new(0.0, CORNER_RESIZE_SIZE), top_left + Vector2::new(CORNER_RESIZE_SIZE, 0.0), gui);
            return;
        }

        let top_right = base.position + Vector2::new(base.size.x, 0.0);
        if (top_right.x - gui.mouse_x as f32) + (gui.mouse_y as f32 - top_right.y) < CORNER_RESIZE_SIZE {
            self.draw_triangle(top_right, top_right - Vector2::new(CORNER_RESIZE_SIZE, 0.0), top_right + Vector2::new(0.0, CORNER_RESIZE_SIZE), gui);
            return;
        }

        let bottom_left = base.position + Vector2::new(0.0, base.size.y);
        if (gui.mouse_x as f32 - bottom_left.x) + (bottom_left.y - gui.mouse_y as f32) < CORNER_RESIZE_SIZE {
            self.draw_triangle(bottom_left, bottom_left + Vector2::new(CORNER_RESIZE_SIZE, 0.0), bottom_left - Vector2::new(0.0, CORNER_RESIZE_SIZE), gui);
            return;
        }*/

        if WindowManager::bottom_right_corner_contains(window_base, gui.mouse_x, gui.mouse_y) {
            let bottom_right = window_base.position + Vector2::new(window_base.size.x, window_base.size.y);
            WindowManager::draw_triangle(
                bottom_right,
                bottom_right - Vector2::new(CORNER_RESIZE_SIZE, 0.0),
                bottom_right - Vector2::new(0.0, CORNER_RESIZE_SIZE),
                gui,
            );
        }
    }

    fn draw_triangle(p0: Vector2, p1: Vector2, p2: Vector2, gui: &mut GuiVariables) {
        let vertex_buffer = &mut gui.draw_list.vertices;
        let index_buffer = &mut gui.draw_list.indices;
        let index_offset = vertex_buffer.len() as u32;

        let color = gui.context.style.window.resize_highlight_color;

        vertex_buffer.push(Vertex::new(p0.x, p0.y, 1.0 / 512.0, 1.0 / 512.0, color));
        vertex_buffer.push(Vertex::new(p1.x, p1.y, 1.0 / 512.0, 1.0 / 512.0, color));
        vertex_buffer.push(Vertex::new(p2.x, p2.y, 1.0 / 512.0, 1.0 / 512.0, color));

        index_buffer.push(index_offset);
        index_buffer.push(index_offset + 1);
        index_buffer.push(index_offset + 2);
    }
}
