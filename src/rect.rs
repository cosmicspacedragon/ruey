use vector2::Vector2;

#[derive(Copy, Clone, Debug)]
pub enum Edge {
    Top = 0,
    Bottom,
    Left,
    Right,
}

#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct Rect {
    pub min: Vector2,
    pub max: Vector2,
}

impl Rect {
    pub fn new(min_x: f32, min_y: f32, max_x: f32, max_y: f32) -> Rect {
        Rect {
            min: Vector2 { x: min_x, y: min_y },
            max: Vector2 { x: max_x, y: max_y },
        }
    }
    pub fn new_size(min_x: f32, min_y: f32, size_x: f32, size_y: f32) -> Rect {
        Rect {
            min: Vector2 { x: min_x, y: min_y },
            max: Vector2 {
                x: min_x + size_x,
                y: min_y + size_y,
            },
        }
    }
    pub fn new_vector(min: Vector2, max: Vector2) -> Rect {
        Rect { min, max }
    }
    pub fn new_size_vector(position: Vector2, size: Vector2) -> Rect {
        Rect {
            min: position,
            max: position + size,
        }
    }
    pub fn new_zero() -> Rect {
        Rect {
            min: Vector2::zero(),
            max: Vector2::zero(),
        }
    }
    pub fn new_intersection(first: Rect, second: Rect) -> Rect {
        let min_x = first.min.x.max(second.min.x);
        let max_x = first.max.x.min(second.max.x);
        let min_y = first.min.y.max(second.min.y);
        let max_y = first.max.y.min(second.max.y);
        Rect {
            min: Vector2::new(min_x, min_y),
            max: Vector2::new(max_x, max_y),
        }
    }

    pub fn contains_or_equal(&self, other: Rect) -> bool {
        (other.min.x >= self.min.x || ((other.min.x - self.min.x).abs() < 0.1))
            && (other.min.y >= self.min.y || ((other.min.y - self.min.y).abs() < 0.1))
            && (other.max.x <= self.max.x || ((other.max.x - self.max.x).abs() < 0.1))
            && (other.max.y <= self.max.y || ((other.max.y - self.max.y).abs() < 0.1))
    }

    pub fn almost_equal(&self, other: Rect) -> bool {
        self.min.almost_equal(other.min) && self.max.almost_equal(other.max)
    }

    pub fn size(&self) -> Vector2 {
        self.max - self.min
    }

    pub fn contains(&self, x: f32, y: f32) -> bool {
        x >= self.min.x && y >= self.min.y && x <= self.max.x && y <= self.max.y
    }

    #[allow(clippy::float_cmp, clippy::collapsible_if)]
    pub fn get_closest_edge(&self, x: f32, y: f32) -> Option<(f32, Edge)> {
        if !self.contains(x, y) {
            return None;
        }

        let x_min_dist = (self.min.x - x).abs();
        let x_max_dist = (x - self.max.x).abs();
        let x_closest = x_min_dist.min(x_max_dist);

        let y_min_dist = (self.min.y - y).abs();
        let y_max_dist = (y - self.max.y).abs();
        let y_closest = y_min_dist.min(y_max_dist);

        let closest = x_closest.min(y_closest);

        if x_closest < y_closest {
            if x_closest == x_min_dist {
                Some((closest, Edge::Left))
            } else {
                Some((closest, Edge::Right))
            }
        } else {
            if y_closest == y_min_dist {
                Some((closest, Edge::Top))
            } else {
                Some((closest, Edge::Bottom))
            }
        }
    }

    pub fn get_closest_edge_triangle(&self, x: f32, y: f32) -> Option<Edge> {
        let top_left = self.min;
        let top_right = Vector2::new(self.max.x, self.min.y);
        let bottom_left = Vector2::new(self.min.x, self.max.y);
        let bottom_right = self.max;
        let center = self.min + self.size() * Vector2::new(0.5, 0.5);

        if point_in_triangle(x, y, top_left, top_right, center) {
            Some(Edge::Top)
        } else if point_in_triangle(x, y, bottom_left, bottom_right, center) {
            Some(Edge::Bottom)
        } else if point_in_triangle(x, y, top_left, bottom_left, center) {
            Some(Edge::Left)
        } else if point_in_triangle(x, y, top_right, bottom_right, center) {
            Some(Edge::Right)
        } else {
            None
        }
    }

    pub fn distance_sq_from_center(&self, x: f32, y: f32) -> f32 {
        let distance = Vector2::new(x, y) - (self.min + self.size() * Vector2::new(0.5, 0.5));
        distance.dot(distance)
    }

    pub fn center(&self) -> Vector2 {
        self.min + self.size() * Vector2::new(0.5, 0.5)
    }
}

// https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle
fn sign(p0: Vector2, p1: Vector2, p2: Vector2) -> f32 {
    (p0.x - p2.x) * (p1.y - p2.y) - (p1.x - p2.x) * (p0.y - p2.y)
}

fn point_in_triangle(x: f32, y: f32, p0: Vector2, p1: Vector2, p2: Vector2) -> bool {
    let pos = Vector2::new(x, y);
    let s0 = sign(pos, p0, p1);
    let s1 = sign(pos, p1, p2);
    let s2 = sign(pos, p2, p0);

    let neg = s0 < 0.0 || s1 < 0.0 || s2 < 0.0;
    let pos = s0 > 0.0 || s1 > 0.0 || s2 > 0.0;

    !(neg && pos)
}
