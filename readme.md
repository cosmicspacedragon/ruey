# Ruey
Ruey is a declarative GUI written completely in Rust, using rust object notation (RON) to declare the GUI layout. The core is written as a platform-agnostic library which only outputs draw lists to the user. These draw lists can then be rendered using any graphics API supporting indexed vertex buffers (or equivalent).

See the wiki for more information about the elements, workflow, and plans.

Quick sample of docking and tabs:
![Sample Video](sample.mp4)

# Version 1.0
In version 1.0 you can expect...

## Docking and tabs
Out-of-the-box support for dockable and/or tabbed windows.

## A number of useful widgets
- Single or multi-line text boxes - with or without the ability to edit them
- Dropdown menus
- Sliders - vertical, horizontal, and with configurable step sizes
- Buttons - normal clickable ones, checkboxes, and radio buttons
- Color pickers - at least RGB and either HSB/HSV or HSL
- Menu bars

## Configurable styling and layout without recompiling
Both style and layout are controlled through external files, so there is never a need to recompile your code for GUI changes.

A goal for 1.0 or shortly thereafter is a diff-based reload, meaning only the affected windows/widgets will be reloaded.

## Easy integration
There will be no need to tie your entire code base to this specific GUI system. Providers allow you to put Ruey on top of your application and reuse your data types and collections.

# Before 1.0
You can expect rough corners, unpolished features, bugs, and breaking changes.

# Core principles
The core principles of Ruey are:
* Real-time capable - The library should always be real-time capable for a "complex but reasonable" UI.
* Fast design iteration - Designing a GUI is a very iterative process, and as such the library should work with the user and make iteration simple.
* Non-invasive - Implementing Ruey in an existing codebase should not require excessive modification or coupling of existing code.
* Dynamic - Dynamic data sources means a dynamic GUI is needed to visualize it.
* Stable and helpful - Invalid inputs to the library should not crash or corrupt it. If an error occurs a helpful error message is given.
* Extendable - GUI elements are self-contained and layouts work with the Element trait, making it easy to add or modify elements.

# How it works
The library is implemented in Rust, but uses RON as a declaration format. The glue between Rust and RON is what is called "providers", they source data from Rust and provide it to the GUI. Each widget is coded to expect certain types of data (strings, i32, etc), and the provider must provide that type.

## Parsing and building
The GUI is created from a given RON file. The file is parsed and elements are created and stored internally, so there is no need to hold onto a handle except for the Gui instance itself.

The GUI system is not yet usable though - it needs to be built first. Building the system goes through all elements and positions them, making them ready for rendering.

### Widgets
A widget is something the user sees: a button, text field, slider etc.

### Layouts
A layout controls placement of gui elements (widgets or layouts). It is possible to nest these to create more complext GUIs.

## Rendering
The only method regularily called is `Element::draw`, and it is called once for each object each frame. This means some updating might be necessary in the `draw` method, since there is no `update` or equivalent. The `draw` method is given (among other things) a list of vertices and indices in which it should place any vertices or indices it needs.

## Styling
The `Context` class contains a `Style` instance, which in turn contains the style to use for any element or layout.

Styling is done through the a RON file, and this file can be reloaded without having to rebuild or reload the entire GUI.

# Controls
Left mouse works as usual (clicking, dragging, resizing). Use right click to activate "docking mode", which will allow docking of windows to other windows. It is possible to undock windows by right clicking the title bar and dragging the window away.

It is currently only possible to resize windows by draggin the bottom-right corner. It is possible to resize docked windows by grabbing the border with the left mouse button.

# Examples
Currently the repo contains an SDL/GL implementation, found in src/main.rs. It will be moved somewhere else in the future, but it is good enough for now.

# Documentation
The documentation is very lack-luster right now because I have been focusing on making Ruey functional.

The wiki exists and might be a bit out-dated, but should be enough to get some insight into the library.

# Contribution
Any help is welcome. Pull requests, testing, suggestions, doubts. Just open up an issue.

This is my first rust project, and the code probably reflects that (and my C/C++ background).

## License
### Ruey
MIT
### CamingoCode
License can be found at https://www.fontsquirrel.com/license/camingocode

